<table class="table table-striped table-bordered table-hover table-responsive table-checkable" id="table_data_units">
				<thead>
					<tr style='text-align:center'>
					  <th>No</th>
						<th>Nama</th>
						<th>Keterangan</th>
						<th>Nilai</th>
						<th>Lapangan</th>
						<th>BAP</th>
						<th>Harga Satuan</th>
						<th>Volume</th>
						<th>Nilai</th>
                        <th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
				  <tr>
				    <td>1</td>
					<td>AA00<?php echo $i; ?>[show/hide]</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
                    <td>&nbsp;</td>
					<td>&nbsp;</td>
                    <td colspan="2" align="center">
                    	<a id="" href="javascript:;" class="btn btn-xs default add-unit-link"> 
                            <i class="fa fa-edit">Unit</i>
                        </a>
                    </td>
				  </tr>
				  <tr>
				    <td style='width:20%'>&nbsp;</td>
					<td style='width:20%'>
					<input type="checkbox" name="item_pekerjaan" id="item_pekerjaan" />
						<label for="item_pekerjaan">&nbsp; Item Pekerjaan Satu</label></td>
					<td><label>Keterangan</label></td>
					<td>
						<label>5,000,000.00</label>
					</td>
					<td><label>100%</label></td>
					<td>
						100%
					</td>
					<td><label>100,000.00</label></td>
                    <td>30 Kg</td>
					<td>3,000,000.00</td>
                    <td align="center">
                        <a id="" href="javascript:;" class="btn btn-xs blue add-edit-link"> 
                            <i class="fa fa-edit">Edit</i>
                        </a>
                    </td>
					<td align="center">
                    	<a id="" href="javascript:;" class="btn btn-xs red add-delete-link"> 
                            <i class="fa fa-times">Delete</i>
                        </a>
                    </td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
					<td><input type="checkbox" name="item_pekerjaan" id="item_pekerjaan" />
						<label for="item_pekerjaan">&nbsp; Item Pekerjaan Dua</label></td>
					<td><label>Keterangan</label></td>
					<td><label>3,000,000.00</label></td>
					<td><label>0%</label></td>
					<td>0%</td>
					<td><label>2,000,000.00</label></td>
                    <td>5 Ton</td>
					<td>10,000,000.00</td>
                    <td align="center">
                        <a id="" href="javascript:;" class="btn btn-xs blue add-unit-link"> 
                            <i class="fa fa-edit">Edit</i>
                        </a>
                    </td>
					<td align="center">
                    	<a id="" href="javascript:;" class="btn btn-xs red add-unit-link"> 
                            <i class="fa fa-times">Delete</i>
                        </a>
                    </td>
				  </tr>
				  <tr>
				    <td>&nbsp;</td>
					<td><input type="checkbox" name="item_pekerjaan" id="item_pekerjaan" />
						<label for="item_pekerjaan">&nbsp; Item Pekerjaan Tiga</label></td>
					<td><label>Keterangan</label></td>
					<td><label>2,500,000.00</label></td>
					<td><label>80%</label></td>
					<td>80%</td>
					<td><label>350,000.00</label></td>
                    <td>1 m</td>
					<td>350,000.00</td>
                    <td align="center">
                        <a id="" href="javascript:;" class="btn btn-xs blue add-unit-link"> 
                            <i class="fa fa-edit">Edit</i>
                        </a>
                    </td>
					<td align="center">
                    	<a id="" href="javascript:;" class="btn btn-xs red add-unit-link"> 
                            <i class="fa fa-times">Delete</i>
                        </a>
                    </td>
				  </tr>
				</table>