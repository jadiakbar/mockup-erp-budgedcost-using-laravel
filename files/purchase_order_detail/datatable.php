<h3 class="page-title"> Purchase Order Detail (purchaseorder_details.no)
</h3>


<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Purchase Order No</th>
			<th>Item Pekerjaan Detail No</th>
			<th>Brand</th>
			<th>Item Satuan</th>
			<th>Quantity</th>
			<th>Quantity Kecil</th>
			<th>Price</th>
			<th>Price Kecil</th>
			<th>PPN (%)</th>
			<th>PPH (%)</th>
			<th>Description</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>(00<?php echo $no; ?>/PO/EST/VI/2017/CN/PRJ)</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

