

<table class='table'>
	<tr>
		<td>Purchase Order No.</td>
		<td>
			<select class='form-control' name='purchase_order_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Purchase Order Detail No.</td>
		<td>
			<select class='form-control' name='purchase_order_detail_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Brand</td>
		<td>
			<select class='form-control' name='brand_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Item Satuan</td>
		<td>
			<select class='form-control' name='item_satuan_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Quantity</td>
		<td>
			<select class='form-control' name='quantity'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Quantity Kecil</td>
		<td>
			<select class='form-control' name='quantity_kecil'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Price</td>
		<td>
			<select class='form-control' name='price'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Price Kecil</td>
		<td>
			<select class='form-control' name='price_kecil'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>PPN (%)</td>
		<td>
			<input type='text' id='ppn' name='ppn' class='form-control' placeholder='PPN (%)' style='width:25%' required />
		</td>
	</tr>
	
	<tr>
		<td>PPH (%)</td>
		<td>
			<input type='text' id='pph' name='pph' class='form-control' placeholder='PPH (%)' style='width:25%' required />
		</td>
	</tr>
	
	<tr>
		<td>Description</td>
		<td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
</table>



<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php // include('multi_select_pluggin.php'); ?>