<h3 class="page-title"> Detail Voucher
</h3>

<table class='table'>
	<tr>
		<td>No. BAP</td>
		<td>
			<!--<input type='text' id='no_bap' name='no_bap' class='form-control' placeholder='No. BAP' required required value='001/BAP/EST/VI/2017/CN/PRJ/001'  /> -->
			<label>001/BAP/EST/VI/2017/CN/PRJ/001</label>
		</td>
	</tr>

	<tr>
		<td>Tanggal</td>
		<td>
			<label>04 October 2017</label>
		</td>
	</tr>
	
	<tr>
		<td>No. Faktur</td>
		<td>
			<label>99999999999999</label>
		</td>
	</tr>
	
	<tr>
		<td>Nilai</td>
		<td style='text-align:right'>
			<!--<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' value='1,500,000.00' />-->
			<label>1,500,000,000.00</label>
			
		</td>
	</tr>
	
	<tr>
		<td>Jatuh Tempo</td>
		<td style='text-align:right'>
			<!--<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' value='1,500,000.00' />-->
			<label>10 November 2017</label>
			
		</td>
	</tr>
	
	<tr>
		<td>Tanggal Penyerahan</td>
		<td style='text-align:right'>
			<!--<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' value='1,500,000.00' />-->
			<label>12 November 2017</label>
			
		</td>
	</tr>
	
	<tr>
		<td>Tanggal Pencairan</td>
		<td style='text-align:right'>
			<!--<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' value='1,500,000.00' />-->
			<label>13 November 2017</label>
			
		</td>
	</tr>
	
	<tr>
		<td style='text-align:right'>Keterangan</td>
		<td>
			-
		</td>
	</tr>
	
	<tr>
		<td></td>
		<td style='text-align:right'>
			<a id="" href="javascript:;" class="btn default add-unit-link"> 
				<i class="fa fa-print">Cetak</i>
			</a>
			
			<a id="" href="javascript:;" class="btn blue add-unit-link"> 
				<i class="fa fa-edit">Edit</i>
			</a>
			
			<a id="" href="javascript:;" class="btn red add-unit-link"> 
				<i class="fa fa-times">Delete</i>
			</a>
		</td>
	</tr>

</table>

<?php include('data_table_units.php'); ?>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>