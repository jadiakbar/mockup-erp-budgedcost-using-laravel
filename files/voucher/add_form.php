<h3 class="page-title"> Create Voucher
</h3>

<table class='table'>
	<tr>
		<td>No. BAP</td>
		<td>
			<!--<input type='text' id='no_bap' name='no_bap' class='form-control' placeholder='No. BAP' required required value='001/BAP/EST/VI/2017/CN/PRJ/001'  /> -->
			<label>001/BAP/EST/VI/2017/CN/PRJ/001</label>
		</td>
	</tr>

	<tr>
		<td>Tanggal</td>
		<td>
			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control" name='date' readonly required value="<?php echo date('d-m-Y'); ?>">
				<span class="input-group-btn">
				<button class="btn default" type="button">
				<i class="fa fa-calendar"></i>
				</button>
				</span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Nilai</td>
		<td>
			<!--<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' value='1,500,000.00' />-->
			<label>1,500,000,000.00</label>
			
		</td>
	</tr>
	
	<tr>
		<td>Jatuh Tempo</td>
		<td>
			<input type='text' id='jatuh_tempo' name='jatuh_tempo' class='form-control' placeholder='Jatuh Tempo' required value='04 December 2017' />
		</td>
	</tr>
	
	<tr>
		<td>Keterangan</td>
		<td>
			<textarea class='form-control' name="keterangan" id="Keterangan" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
	<tr>
		<td>Rekanan</td>
		<td>
			<label id='rekanan'>PT. Satu Prima</label>
		</td>
	</tr>
	
	<tr>
		<td>Rekening Pembayaran</td>
		<td>
			<input type='text' id='rek_pembayaran' name='rek_pembayaran' class='form-control' placeholder='Rekening Pembayaran' required value='BCA 5370000000 Satu Prima, PT' />
		</td>
	</tr>

</table>

<?php include('datatable_pluggin.php'); ?>
<?php include('data_table_units.php'); ?>
<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#table_data_units').DataTable();
			$('#table_data_units')
			.removeClass( 'display' )
			.addClass('table table-bordered');
		});
	</script>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php include('datetimepicker_pluggin.php'); ?>