<table class='table'>
	<tr>
		<td>Purchase Order</td>
		<td>
			<select class='form-control' name='purchaseorder_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>

	<tr>
		<td>Good Receive No</td>
		<td>
			<input type='text' id='no' name='no' class='form-control' placeholder='Good Receive No.' required />
		</td>
	</tr>
	
	<tr>
		<td>Tanggal</td>
		<td>
			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control" name='date' readonly required value="<?php echo date('d-m-Y'); ?>">
				<span class="input-group-btn">
				<button class="btn default" type="button">
				<i class="fa fa-calendar"></i>
				</button>
				</span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Status</td>
		<td>
			<select class='form-control' name='status' style='widht:30%'>
				<option>-- CHOOSE --</option>
				<option>Open</option>
				<option>Release</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Is DownPayment</td>
		<td>
			<input type='checkbox' name='is_downpayment' checked />
		</td>
	</tr>
	
</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php include('datetimepicker_pluggin.php'); ?>