<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Good Receive No</th>
			<th>Tanggal</th>
			<th>Status</th>
			<th>Is DownPayment</th>
			<th>View</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>(GR00<?php echo $no; ?>)</td>
			<td>...</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo $open;
				}
				else {
					echo $release;
				}
				?>
			</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo 'Yes';
				}
				else if(in_array($no, array(2,5,9), TRUE)) {
					echo 'No';
				}
				else if(in_array($no, array(3,7), TRUE)) {
					echo 'Yes';
				}
				else {
					echo 'No';
				}
				?>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">&nbsp;Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">&nbsp;Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

<!--
<br />
Information : 
<br />
<div style='widht:30%'>
<?php
// echo '<div style="widht:30%">Approved <br />'.$approved.'</div>';
// echo '<div style="widht:30%">In Progress <br />'.$in_progress.'</div>';
// echo '<div style="widht:30%">Rejected <br />'.$rejected.'</div>';
?>
</div>
-->
