<h3 class="page-title"> Create Berita Acara Prestasi
</h3>

<table class='table'>
	<tr>
		<td>No. SPK</td>
		<td>
			<div class="form-group">
				<div class="col-md-9">
					<select multiple="multiple" class="multi-select" id="my_multi_select1" name="no_spk[]" onchange='setTermin()'>
					<?php
					$no = 1;
					for($no = 1; $no <= 10; $no++)
					{
						?>
						<option value='#<?php echo $no; ?>'>
						00<?php echo $no; ?>/SPK/EST/VI/2017/CN/PRJ</option>
						<?php
					}
					?>
					</select>
				</div>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Tanggal</td>
		<td>
			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control" name='tanggal' readonly required value="<?php echo date('d-m-Y'); ?>" />
					<span class="input-group-btn">
				<button class="btn default" type="button">
					<i class="fa fa-calendar"></i>
				</button>
				</span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Termin</td>
		<td>
			<input type='text' id='termin' name='termin' class='form-control' placeholder='No. BAP' required required disabled  /> 
			<script type="text/javascript">
			function setTermin() {
				//Get selected option of the HTML SELECT
				var selectedItem = $("#my_multi_select1 option:selected").last();
				document.getElementById('termin').value += selectedItem.val()+','; 
			};
			</script>
		</td>
	</tr>
	
	<tr>
		<td>Progress Termin Sebelumnya</td>
		<td>
			0%
		</td>
	</tr>
	
	<tr>
		<td>Progress Termin Sekarang</td>
		<td>
			42%
		</td>
	</tr>
	
	<tr>
		<td>Progress Lapangan</td>
		<td>
			45%
		</td>
	</tr>
	
	<tr>
		<td>Nilai SPK</td>
		<td align='right'>
			<!--<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' value='1,500,000.00' />-->
			<label>125,000,000.00</label>
			
		</td>
	</tr>
	
	<tr>
		<td>Nilai BAP</td>
		<td align='right'>
			<!--<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' value='1,500,000.00' />-->
			<label>52,500,000.00</label>
			
		</td>
	</tr>
	
	<tr>
		<td><strong><h3>Unit</h3></strong></td>
		<td>
		
		</td>
	</tr>
	
	<tr>
		<td colspan='2'>
			<?php
				for($i = 1; $i <= 3; $i++) {
			?>
			<table>
				<tr>
				<td>
					<a onclick='hideUnit<?php echo $i; ?>()'>AA00<?php echo $i; ?>[show/hide]</a>
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
			
			<table class="table table-striped table-bordered table-hover table-responsive table-checkable" id="table_data_units<?php echo $i; ?>">
				  <tr>
					<td style='width:40%'><label for="item_pekerjaan">&nbsp; Item Pekerjaan Satu<?php echo $no; ?></label></td>
					<td>80%</td>
					<td><input type="text" class='form-control' name="nilai_real" id="nilai_real" placeholder='Nilai Real' value='75' /></td>
					<td>%</td>
				  </tr>
				  <tr>
					<td><label for="item_pekerjaan">&nbsp; Item Pekerjaan Dua<?php echo $no; ?></label></td>
					<td>50%</td>
					<td><input type="text" class='form-control' name="nilai_real" id="nilai_real" placeholder='Nilai Real' value='45' /></td>
					<td>%</td>
				  </tr>
				  <tr>
					<td><label for="item_pekerjaan">&nbsp; Item Pekerjaan Tiga<?php echo $no; ?></label></td>
					<td>10%</td>
					<td><input type="text" class='form-control' name="nilai_real" id="nilai_real" placeholder='Nilai Real' value='10' /></td>
					<td>%</td>
				  </tr>
			</table>
			<script>
			function hideUnit<?php echo $i; ?>() {
				var table_data_units = document.getElementById('table_data_units<?php echo $i; ?>');
				
				var hide = table_data_units.style.display =="none";
				if (hide) {
					table_data_units.style.display="table";
				} 
				else {
					table_data_units.style.display="none";
				}
			}
			</script>
			
			<?php
			}
			?>
		</td>
	</tr>
	
</table>

<?php include('multi_select_pluggin.php'); ?>
<?php include('datetimepicker_pluggin.php'); ?>
<script type="text/javascript" charset="utf-8">
/*
	$(document).ready(function() {
		$('#table_data_units').DataTable();
		$('#table_data_units')
		.removeClass( 'display' )
		.addClass('table table-bordered');
	});
	*/
</script>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php include('multi_select_pluggin.php'); ?>