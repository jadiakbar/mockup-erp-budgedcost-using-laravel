<h3 class="page-title"> Detail Berita Acara Prestasi
</h3>

<table class='table'>
	<tr>
		<td>No. BAP</td>
		<td>
			<label>101/BAP/EST/VI/2017/CN/PRJ</label>
		</td>
	</tr>
	
	<tr>
		<td>No. SPK</td>
		<td>
			<label>001/SPK/EST/VI/2017/CN/PRJ</label> 
		</td>
	</tr>
	
	<tr>
		<td>Tanggal</td>
		<td>
			<label><?php echo date("d/m/Y"); ?></label>
		</td>
	</tr>
	
	<tr>
		<td>Termin</td>
		<td>
			#1
		</td>
	</tr>

	<tr>
		<td>Progress Lapangan</td>
		<td>
			45%
		</td>
	</tr>
	
	<tr>
		<td>Progress Pembayaran</td>
		<td>
			39%
		</td>
	</tr>
	
	<tr>
		<td>Nilai SPK</td>
		<td align='right'>
			<!--<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' value='1,500,000.00' />-->
			<label>125,000,000.00</label>
			
		</td>
	</tr>
	
	<tr>
		<td>Nilai BAP</td>
		<td align='right'>
			<!--<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' value='1,500,000.00' />-->
			<label>52,500,000.00</label>
			
		</td>
	</tr>
	
	<tr>
		<td><strong><h3>Unit</h3></strong></td>
		<td>
			
		</td>
	</tr>
	
	<tr>
		<td colspan='2'>
			<table class="table table-striped table-bordered table-hover table-responsive table-checkable" id="table_data_units">
			<thead>
				<tr>
					<td>Unit Code</td>
					<td>Lapangan</td>
					<td>BAP</td>
				</tr>
			</thead>
			<tbody>
				<?php
				for($i = 1; $i <= 3; $i++) {
				?>
					<tr>
						<td><strong>AA00<?php echo $i; ?>[show/hide]</strong></td>
						<td>&nbsp;</td>
						<td></td>
					</tr>
				  <tr>
					<td style='width:40%'><label for="item_pekerjaan">&nbsp; Item Pekerjaan Satu</label></td>
					<td>80%</td>
					<td>75%</td>
				  </tr>
				  <tr>
					<td><label for="item_pekerjaan">&nbsp; Item Pekerjaan Dua</label></td>
					<td>50%</td>
					<td>45%</td>
				  </tr>
				  <tr>
					<td><label for="item_pekerjaan">&nbsp; Item Pekerjaan Tiga</label></td>
					<td>10%</td>
					<td>10%</td>
				  </tr>
				<?php
				}
				?>
				<tr>
					<td style='text-align:right' colspan='3'>
						<a id="" href="javascript:;" class="btn default add-unit-link"> 
							<i class="fa fa-print">Cetak</i>
						</a>
						
						<a id="" href="javascript:;" class="btn blue add-unit-link"> 
							<i class="fa fa-edit">Edit</i>
						</a>
						
						<a id="" href="javascript:;" class="btn red add-unit-link"> 
							<i class="fa fa-times">Delete</i>
						</a>
					</td>
				</tr>
			</tbody>
			</table>
		</td>
	</tr>
	
</table>


<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#table_data_units').DataTable();
			$('#table_data_units')
			.removeClass( 'display' )
			.addClass('table table-bordered');
		});
	</script>

<!--
<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>
-->