
<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Type</th>
			<th>Lapangan</th>
			<th>BAP</th>
			<th>Start</th>
			<th>Finish</th>
			<th>Nilai</th>
			<th>Detail</th>
			<th>SPK</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		for($no = 1; $no <= 25; $no++)
		{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>AA00<?php echo $no; ?></td>
			<td>(types.name<?php echo $no; ?>)</td>
			<td align='right'>
				<?php if(($no % 2) == 0) {
						echo '50%';
					}
					else if(in_array($no, array(2,5,9), TRUE)) {
						echo '0%';
					}
					else {
						echo '80%';
					}
				?>
			</td>
			<td align='right'>
				<?php if(($no % 2) == 0) {
						echo '20%';
					}
					else if(in_array($no, array(2,5,9), TRUE)) {
						echo '0%';
					}
					else {
						echo '50%';
					}
				?>
			</td>
			<td>
				<?php
					echo $no.date('m-Y');
				?>
			</td>
			<td>
				<?php
					echo $no.date('m-Y');
				?>
			</td>
			<td align='right'>
				<?php if(($no % 2) == 0) {
						echo '120,000,000.00';
					}
					else if(in_array($no, array(2,5,9), TRUE)) {
						echo '100,000,000.00';
					}
					else {
						echo '150,000,000.00';
					}
				?>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue detail-link"> 
					<i class="fa fa-edit">Detail</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue spk-link"> 
					<i class="fa fa-newspaper-o">&nbsp;Spk</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

