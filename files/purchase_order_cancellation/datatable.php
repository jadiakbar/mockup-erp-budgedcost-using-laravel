<h3 class="page-title"> Purchase Order (purchaseorder_details.purchaseorder_id)
</h3>



<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Purchase Cancel No</th>
			<th>Tanggal</th>
			<th>Status</th>
			<th>Description</th>
			<th>Detail</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>(00<?php echo $no; ?>/POC/EST/VI/2017/CN/PRJ)</td>
			<td>...</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo $finnish;
				}
				else {
					echo $processing;
				}
				?>
			</td>
			<td>...</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple detail-link"> 
					<i class="fa fa-newspaper-o">&nbsp;Detail</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

