

<table class='table'>
	<tr>
		<td>Purchase Request No.</td>
		<td>
			<select class='form-control' name='purchase_request_no'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Cancel No.</td>
		<td>
			<input type='text' id='cancel_no' name='cancel_no' class='form-control' placeholder='Cancel No.' required />
		</td>
	</tr>
	
	<tr>
		<td>Tanggal</td>
		<td>
			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control" name='date' readonly required value="<?php echo date('d-m-Y'); ?>">
				<span class="input-group-btn">
				<button class="btn default" type="button">
				<i class="fa fa-calendar"></i>
				</button>
				</span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Status</td>
		<td>
			<select class='form-control' name='status'>
				<option>-- CHOOSE --</option>
				<option>Processing</option>
				<option>Finnish</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Description</td>
		<td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
</table>



<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php include('datetimepicker_pluggin.php'); ?>