<?php include('datatable_pluggin.php'); ?>

<div id="div_content_page">
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btn-view").hide();
			
			$("#btn-add").click(function(){
				$(".content-loader").fadeOut('slow', function()
				{
					$(".content-loader").fadeIn('slow');
					$(".content-loader").load('spk_detail/add_form.php');
					$("#btn-add").hide();
					$("#btn-view").show();
				});
			});
			
			$("#btn-view").click(function(){
				
				$("#div_content_page").fadeOut('slow', function()
				{
					$("#div_content_page").load('spk_detail/index.php');
					$("#div_content_page").fadeIn('slow');
				});
			});
			
			$("#btn-close").click(function(){
				
				$("#div_content_page").fadeOut('slow', function()
				{
					$("#div_content_page").load('spk/index.php');
					$("#div_content_page").fadeIn('slow');
				});
			});
		});
	</script>




	<div class="portlet-body">
		<h3 class="page-title"> SPK Details (spks.no)
			<small>spk details information</small>
		</h3>
		<hr />
			<button class="btn sbold green" type="button" id="btn-add"> <span class="glyphicon glyphicon-pencil"></span> &nbsp; Add Data</button>
			<button class="btn btn-info" type="button" id="btn-view" style="display:none"> <span class="glyphicon glyphicon-eye-open"></span> &nbsp; View Data</button>
			<?php if (isset($_GET['view_id'])) {
			?>
				<button class="btn dark" type="button" id="btn-close" style="display:"> <span class="glyphicon glyphicon-off"></span> &nbsp; Close</button>
			<?php
			}
			?>
		<hr />
		
		<div id="div_message">
			<!-- here message will be displayed -->
		</div>
		
		<div class="content-loader">
			<?php include('datatable.php'); ?>
		</div>
    </div>
    <br />

	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#table_data').DataTable();
			$('#table_data')
			.removeClass( 'display' )
			.addClass('table table-bordered');
		});
	</script>
</div>


<div id="div_ajax_func"> 
	<?php include('ajax_load.php'); ?> 
</div>


