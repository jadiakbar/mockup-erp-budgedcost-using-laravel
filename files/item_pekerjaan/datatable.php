<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Code</th>
			<th>Item Pekerjaan</th>
			<th>PPN (%)</th>
			<th>Tag</th>
			<th>Descriptions</th>
			<th>Coa</th>
			<th>View</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>00<?php echo $no; ?></td>
			<td>Item Pekerjaan <?php echo $no; ?></td>
			<td style="text-align:right">
				<?php if(($no % 2) == 0) {
					echo '10';
				}
				else {
					echo '15';
				}
				?>
			</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo '0';
				}
				else {
					echo '1';
				}
				?>
			</td>
			<td>-</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple list-link"> 
					<i class="glyphicon glyphicon-list-alt">&nbsp;Items</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">&nbsp;Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">&nbsp;Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

