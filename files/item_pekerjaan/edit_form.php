<table class='table'>
	<tr>
		<td>Kode Pekerjaan </td>
		<td>
			<input type='text' id='kode_pekerjaan' name='kode_pekerjaan' class='form-control' placeholder='Kode Pekerjaan' required style="width:50%" />
		</td>
	</tr>
	
	<tr>
		<td>Item Pekerjaan </td>
		<td>
			<input type='text' id='item_pekerjaan' name='item_pekerjaan' class='form-control' placeholder='Item Pekerjaan' required />
		</td>
	</tr>
	
	<tr>
		<td>PPN (%) </td>
		<td>
			<input type='text' id='ppn' name='ppn' class='form-control' placeholder='PPN' required style="width:20%" />
		</td>
	</tr>
	
	<tr>
		<td>Tag </td>
		<td>
			<input type='text' id='tag' name='tag' class='form-control' placeholder='Tag' required style="width:20%" />
			<br />
			Tag Values : 
			<br />
			0 : Item Pekerjaan Unit
			<br />
			1 : Item Pekerjaan Non Unit
			<br />
			2 : Others
			<br />
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>