<table class='table'>
	<tr>
		<td>Nama Rekanan</td>
		<td>
			<select class='form-control' name='nama_rekanan'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>PT. Ciputra Group</td>
		<td>
			<select class='form-control' name='pt_ciputra_group'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>No. SUPP</td>
		<td>
			<input type='text' id='no_supp' name='no_supp' class='form-control' placeholder='No. SUPP' required style='20%' />
		</td>
	</tr>
	
	<tr>
		<td>Tanggal SUPP</td>
		<td>
			<select class='form-control' name='tgl_supp'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Penandatanganan</td>
		<td>
			<select class='form-control' name='tgl_supp'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Saksi</td>
		<td>
			<select class='form-control' name='saksi'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Tanggal Cetak</td>
		<td>
			<select class='form-control' name='tgl_cetak'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Tanggal Setuju</td>
		<td>
			<select class='form-control' name='tgl_setuju'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>SUPP Template ID</td>
		<td>
			<select class='form-control' name='supp_template_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Status SUPP</td>
		<td>
			<select class='form-control' name='status_supp'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descritions" id="Descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>