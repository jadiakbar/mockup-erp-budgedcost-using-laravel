<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode</th>
			<th>Rekanan</th>
			<th>PT. Ciputra Group</th>
			<th>No. SUPP</th>
			<th>Tanggal SUPP</th>
			<th>Penandatanganan</th>
			<th>Saksi</th>
			<th>Tanggal Cetak</th>
			<th>Tanggal Setuju</th>
			<th>Status</th>
			<th>SUPP Template</th>
			<th>Descriptions</th>
			<th>View</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td>1</td>
			<td>00<?php echo $no; ?></td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td><user.name></td>
			<td><user.name></td>
			<td>...</td>
			<td>...</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo 'Active';
				}
				else {
					echo '<span style="color:red">Non-Active</span>';
				}
				?>
			</td>
			<td>...</td>
			<td>-</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

