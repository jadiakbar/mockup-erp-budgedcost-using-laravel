<table class='table'>
	<tr>
		<td>COA </td>
		<td>
			<input type='text' id='coa_number' name='coa_number' class='form-control' placeholder='COA Number' required />
		</td>
	</tr>
	
	<tr>
		<td>COA Parent</td>
		<td>
			<select class='form-control' name="coa_parent">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>