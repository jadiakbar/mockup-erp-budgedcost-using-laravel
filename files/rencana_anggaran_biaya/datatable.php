<h3 class="page-title">RAB for WO (workorders.no)
</h3>

<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>RAB No.</th>
			<th>Description</th>
			<th>RAB Flow</th>
			<th>WO No.</th>
			<th>Note</th>
			<th>Status</th>
			<th>Detail</th> 
			<th>Edit</th>
			<th>Delete</th>
			<th>Sirkulasi</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$no = 1;
		for($no = 1; $no <= 25; $no++)
		{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>(rabs.no<?php echo $no; ?>)</td>
			<td>(rabs.description<?php echo $no; ?>)</td>
			<td>...</td>
			<td>(workorders.no<?php echo $no; ?>)</td>
			<td>(rabs.note<?php echo $no; ?>)</td>
			<td>
			
				<?php if(($no % 2) == 0) {
					echo $approved;
				}
				else if(in_array($no, array(2,5,9), TRUE)) {
					echo $in_progress;
				}
				else {
					echo $rejected;
				}
				?>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple detail-link"> 
					<i class="fa fa-newspaper-o">&nbsp;Detail</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">&nbsp;Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">&nbsp;Delete</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs yellow sirkulasi-link">
					<i class="fa-sticky-note">&nbsp;Sirkulasi</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

