<h3 class="page-title"> Detail Nama Supp Rekanan Revision 1
</h3>

<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>ID Rev.</th>
			<th>ID Parent</th>
			<th>No. SUPP Revision</th>
			<th>Is Active</th>
			<th>Content</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	for($no = 1; $no <= 15; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>SUPP000<?php echo $no; ?>R</td>
			<td>SUPP000<?php echo $no; ?>	</td>
			<td><?php echo'<supp_revisions.no>'; ?></td>
			<td>...</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo 'Active';
				}
				else {
					echo '<span style="color:red">Non-Active</span>';
				}
				?>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">&nbsp;Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">&nbsp;Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

