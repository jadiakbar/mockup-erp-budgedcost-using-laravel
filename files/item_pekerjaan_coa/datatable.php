<h3 class="page-title"> Details Item Pekerjaan 1
</h3>

<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Code</th>
			<th>Item Pekerjaan</th>
			<th>Department</th>
			<th>COA</th>
			<th>COA Name</th>
			<th>Descriptions</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	for($no = 1; $no <= 15; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>001</td>
			<td>Item Pekerjaan 1</td>
			<td>Department Name <?php echo $no; ?></td>
			<td>11.41.21<?php echo $no; ?></td>
			<td>Biaya Desain dan Rencana</td>
			<td>-</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">&nbsp;Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">&nbsp;Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

