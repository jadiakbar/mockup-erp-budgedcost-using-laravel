<table class='table'>
	<tr>
		<td>Item Pekerjaan </td>
		<td>
			<input type='text' id='item_pekerjaan' name='item_pekerjaan' class='form-control' placeholder='Item Pekerjaan Name' required />
		</td>
	</tr>
	
	<tr>
		<td>Department</td>
		<td>
			<select class='form-control' name="department">
				<option>-- CHOOSE --</option>
				<option>Department Name 1</option>
				<option>Department Name 2</option>
				<option>Department Name 3</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>COA </td>
		<td>
			<select class='form-control' name="satuan">
				<option>-- CHOOSE --</option>
				<option>+01.00.XX.XXXX</option>
				<option>+02.00.XX.XXXX</option>
				<option>+03.00.XX.XXXX</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>