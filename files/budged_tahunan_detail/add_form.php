
<table class='table'>
	<tr>
		<td>Budged Tahunan</td>
		<td>
			<label>(budged.no)</label>
		</td>
	</tr>
	
	<tr>
		<td>Tahun Anggaran</td>
		<td>
			<label>(budgedtahunans.tahun_anggaran)</label>
		</td>
	</tr>

	<tr>
		<td>Search Item</td>
		<td>
			<!--<input type='text' id='search_item' name='search_item' class='form-control' placeholder='No. Budged Global' required />
			-->
			<div class="input-icon right">
				<i class="fa fa-search fa-spin font-blue"></i>
				<input class="form-control" placeholder="Right icon" type="text"> </div>
		</td>
	</tr>
	
	<tr>
		<td>Item Pekerjaan</td>
		<td>
			<div class="form-group">
				<div class="col-md-9">
					<select multiple="multiple" class="multi-select" id="my_multi_select1" name="item_pekerjaan[]">
					<?php
					$no = 1;
					for($no = 1; $no <= 10; $no++)
					{
						?>
						<option>
						(itempekerjaans.name<?php echo $no; ?>)</option>
						<?php
					}
					?>
					</select>
				</div>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Nilai</td>
		<td>
			<input type='text' id='nilai' name='nilai' class='form-control' placeholder='Nilai' required style='width:30%' />
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php include('multi_select_pluggin.php'); ?>