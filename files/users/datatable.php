<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>PT</th>
			<th>Dept.</th>
			<th>Dev</th>
			<th>Jabatan</th>
			<th>Level</th>
			<th>Can Approve</th>
			<th>Descriptions</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>PT 0<?php echo $no; ?></td>
			<td>DEP 0<?php echo $no; ?></td>
			<td>DEV 0<?php echo $no; ?></td>
			<td>-</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo '1';
				}
				else {
					echo '2';
				}
				?>
			</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo '1';
				}
				else {
					echo '0';
				}
				?>
			</td>
			<td>-</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

