<div class="tabbable tabbable-tabdrop">
	<ul class="nav nav-pills">
		<li class="active">
			<a href="#tab1" data-toggle="tab">User</a>
		</li>
		<li>
			<a href="#tab2" data-toggle="tab">User Details</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<p>
				<table class='table'>
					<tr>
						<td>Login</td>
						<td>
							<input type='text' id='username' name='username' class='form-control' placeholder='Username' required />
						</td>
					</tr>
				
					<tr>
						<td>Name</td>
						<td>
							<input type='text' id='name' name='name' class='form-control' placeholder='Full Name' required />
						</td>
					</tr>
					
					<tr>
						<td>User Group</td>
						<td>
							<select class='form-control' name="user_group">
								<option>-- CHOOSE --</option>
							</select>
						</td>
					</tr>
			 
					<tr>
						<td>Email</td>
						<td>
							<input type='text' id='user_email' name='user_email' class='form-control' placeholder='email@email.com' required>
						</td>
					</tr>
					
					<!--
					<tr>
						<td>Phone</td>
						<td>
							<input type='text' id='user_phone' name='user_phone' class='form-control' placeholder='Phone Number' required>
						</td>
					</tr>
					-->
					
					<tr>
						<td>Digital Signatures</td>
						<td>
							<input type='file' id='digitalsignature' name='digitalsignature' class='form-control' placeholder='Digital Signatures' required>
						</td>
					</tr>
					
					<!--
					<tr>
						<td>Images</td>
						<td>
							<input type='file' id='photo' name='photo' class='form-control' placeholder='Digital Signatures' required>
						</td>
					</tr>
					-->
					
					<!--
					<tr>
						<td>Password</td>
						<td>
							<input type='password' id='password' name='password' class='form-control' placeholder='Your Password' required>
						</td>
					</tr>
					-->
					
					<tr>
						<td>Descriptions</td>
					  <td>
							<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
						</td>
					</tr>
			 
				</table>
			</p>
		</div>
		<div class="tab-pane" id="tab2">
			<p>
				<table class='table'>
					<tr>
						<td>Perusahaan</td>
						<td>
							<select class='form-control' name="mappingperusahaan_id">
							  <option value="101">PT. ABC</option>
							  <option value="102">PT. Kediri</option>
							  <option value="103">PT. Mojokerto</option>
							</select>
						</td>
					</tr>
				
					<tr>
						<td>Jabatan</td>
						<td>
							<select class='form-control' name="user_level">
							  <option value="1">Manager</option>
							  <option value="2">Supervisor</option>
							  <option value="3">Pegawai Lapangan</option>
						  </select>
						</td>
					</tr>
			 
					<tr>
						<td>Can Approve</td>
						<td><input class='checker' type="checkbox" name="can_approve" id="can_approve">
						<label for="can_approve"></label></td>
					</tr>
					
					<tr>
						<td>Level</td>
						<td><select class='form-control' name="user_level">
						  <option value="1">Super Administrator</option>
						  <option value="2">Administrator</option>
						  <option value="3">Employee</option>
						</select></td>
					</tr>
					
					<tr>
						<td>Descriptions</td>
					  <td>
							<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
						</td>
					</tr>
				</table>
			</p>
		</div>
	</div>
</div>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>