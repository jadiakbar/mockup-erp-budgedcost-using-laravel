<?php if(isset($_GET['view_id'])) { ?>
<h3 class="page-title"> Detail Project Kawasan Name 1
</h3>
<?php } ?>

<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Code</th>
			<th>Name</th>
			<th>Luas Lahan</th>
			<th>HPP Tanah/meter</th>
			<th>Kawasan</th>
			<th>Project Name</th>
			<th>Descriptions</th>
			<th>View</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>BLK00<?php echo $no; ?></td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>(project_kawasan.name)</td>
			<td>(project.name)</td>
			<td>-</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

