<table class='table'>
	<tr>
		<td>Kawasan</td>
		<td>
			<select class='form-control' name='project_kawasan_name'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Code</td>
		<td>
			<input type='text' id='code' name='code' class='form-control' placeholder='Code' required />
		</td>
	</tr>

	<tr>
		<td>Name</td>
		<td>
			<input type='text' id='name' name='name' class='form-control' placeholder='Name' required />
		</td>
	</tr>
	
	<tr>
		<td>Luas Lahan</td>
		<td>
			<input type='text' id='luas_lahan' name='luas_lahan' class='form-control' placeholder='Luas' required />
		</td>
	</tr>
	
	<tr>
		<td>HPP Tanah/Meter</td>
		<td>
			<input type='text' id='hpp_tanah' name='hpp_tanah' class='form-control' placeholder='HPP Tanah' required />
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>