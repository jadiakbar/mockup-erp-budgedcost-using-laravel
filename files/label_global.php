<?php 
$date_format = date("Y-m-d h:i:sa");

$open = '<div class="alert-info fade in" style="text-align:center;background-color:#b3ffb3;color:#cc7a00">Open</div>';
$approved = '<div class="alert-success fade in" style="text-align:center">Approved</div>';
$partial = '<div class="alert-info fade in" style="text-align:center;background-color:#ffff99;color:#00b3b3">Partial</div>';
$closed = '<div class="alert-warning fade in" style="text-align:center;background-color:#1f1f14;color:#ffffcc">Closed</div>';

$paid = '<div class="alert-info fade in" style="text-align:center;background-color:#ccccff;color:#0000b3">Paid</div>';
$approved = '<div class="alert-success fade in" style="text-align:center">Approved</div>';

$in_progress = '<div class="alert-warning fade in" style="text-align:center">In Progress</div>';
$rejected = '<div class="alert-danger fade in" style="text-align:center">Rejected</div>';

$bertambah = '<div class="alert-success fade in" style="text-align:center" style="text-align:center;background-color:#99e6ff;color:#ffffb3">Bertambah</div>';
$berkurang = '<div class="alert-danger fade in" style="text-align:center" style="text-align:center;background-color:#ffb3d1;color:#cce6ff">Berkurang</div>';

$processing = '<div class="alert-info fade in" style="text-align:center;background-color:#ffff99;color:#00b3b3">Processing</div>';
$finnish = '<div class="alert-warning fade in" style="text-align:center;background-color:#1f1f14;color:#ffffcc">Finnish</div>';

$release = '<div class="alert-info fade in" style="text-align:center;background-color:#b3e6ff;color:#005580">Release</div>';

$cash = '<div style="color:#009900">Cash</div>';
$credit = '<div style="color:#b35900">Credit</div>';
$transfer = '<div style="color:#007a99">Trensfer</div>';
$cek = '<div style="color:#24248f">Cek</div>';
$giro = '<div style="color:#730099">Giro</div>';
?>