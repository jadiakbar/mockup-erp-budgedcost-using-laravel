<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="../assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../js/table-data-function.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN SIDEBAR MENU -->
<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
	<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
	<li class="sidebar-toggler-wrapper hide">
		<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
		<div class="sidebar-toggler"> </div>
		<!-- END SIDEBAR TOGGLER BUTTON -->
	</li>
	<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
	<li class="sidebar-search-wrapper">
		<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
		<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
		<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
		<form class="sidebar-search  " action="page_general_search_3.html" method="POST">
			<a href="javascript:;" class="remove">
				<i class="icon-close"></i>
			</a>
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<a href="javascript:;" class="btn submit">
						<i class="icon-magnifier"></i>
					</a>
				</span>
			</div>
		</form>
		<!-- END RESPONSIVE QUICK SEARCH FORM -->
	</li>
	<li class="heading">
		<h3 class="uppercase">MAIN</h3>
	</li>
	<li class="nav-item start active open">
		<a href="javascript:;" class="nav-link nav-toggle">
			<i class="icon-home"></i>
			<span class="title">Dashboard</span>
			<span class="selected"></span>
			<span class="arrow open"></span>
		</a>
		<ul class="sub-menu">
			<li class="nav-item start active open">
				<a href="" class="nav-link ">
					<i class="icon-bar-chart"></i>
					<span class="title">Dashboard 1</span>
					<span class="selected"></span>
				</a>
			</li>
		</ul>
	</li>
	
	<?php
	$menu_access = $_GET['menu_access'];
	if($menu_access == 'administrasi') {
	?>
		
	<li class="nav-item ">
		<a href="javascript:;" class="nav-link nav-toggle">
			<i class="icon-docs"></i>
			<span class="title">MASTER DATA</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="nav-item  ">
				<a id="a_users" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">User</span>
				</a>
			</li>
			<li class="nav-item ">
				<a id="a_user_groups"  class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">User Groups</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_item_pekerjaan"  class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Item Pekerjaan</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_barang" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Barang</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Rekanan</span>
					<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item  ">
						<a id="a_profile_rekanan"  class="nav-link ">
							<i class="fa fa-sticky-note-o"></i>
							<span class="title">Profile</span>
						</a>
					</li>
					<li class="nav-item  ">
						<a id="a_supp"  class="nav-link ">
							<i class="fa fa-sticky-note-o"></i>
							<span class="title">SUPP</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="nav-item  ">
				<a id="a_pt" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">PT</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_department" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Department</span>
				</a>
			</li>
		</ul>
	</li>
		
	<li class="nav-item ">
		<a href="javascript:;" class="nav-link nav-toggle">
			<i class="icon-docs"></i>
			<span class="title">PLANNING</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="nav-item  ">
				<a id="a_project" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Project</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_kawasan" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Kawasan</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_project_blocks" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Blok</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_unit" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Unit</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_sub_unit" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Sub Unit</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_tipe_unit" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Tipe Unit</span>
				</a>
			</li>
		</ul>
	</li>
	
	<li class="nav-item ">
		<a href="javascript:;" class="nav-link nav-toggle">
			<i class="icon-docs"></i>
			<span class="title">BUDGETTING</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="nav-item  ">
				<a id="a_budged_total" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Budged Total</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_budged_tahunan" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Budged Tahunan</span>
				</a>
			</li>
		</ul>
	</li>
			
	

	<li class="nav-item ">
		<a href="javascript:;" class="nav-link nav-toggle">
			<i class="icon-docs"></i>
			<span class="title">PEMBELIAN</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">	
			<li class="nav-item  ">
				<a id="a_purchase_request" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Purchase Request (PR)</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_purchase_order" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Purchase Order (PO)</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_penerimaan_barang" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Penerimaan Barang</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_good_receive" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Good Receive (GR)</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_retur" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Retur</span>
				</a>
			</li>
		</ul>
	</li>
	
	
		
		
	<li class="nav-item ">
		<a href="javascript:;" class="nav-link nav-toggle">
			<i class="icon-docs"></i>
			<span class="title">INVENTORY</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="nav-item  ">
				<a id="a_inventarisasi" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Inventarisasi</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_pemakaian_barang" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Pemakaian Barang</span>
				</a>
			</li>	
		</ul>
	</li>

		
		
	
	
	<li class="nav-item ">
		<a href="javascript:;" class="nav-link nav-toggle">
			<i class="icon-docs"></i>
			<span class="title">PEKERJAAN</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="nav-item  ">
				<a id="a_work_order" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Work Order (WO)</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_rencana_anggaran_biaya" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Rencana Anggaran Biaya (RAB)</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_pra_spk" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Pra SPK</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_spk" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Surat Perintah Kerja (SPK)</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Surat Instruksi</span>
					<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item  ">
						<a id="a_sik"  class="nav-link ">
							<i class="fa fa-sticky-note-o"></i>
							<span class="title">SIK</span>
						</a>
					</li>
					<li class="nav-item  ">
						<a id="a_sil"  class="nav-link ">
							<i class="fa fa-sticky-note-o"></i>
							<span class="title">SIL</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="nav-item  ">
				<a id="a_variation_order" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Variation Order (VO)</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_progress_lapangan" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Progress Lapangan</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_berita_acara_prestasi" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Berita Acara Prestasi (BAP)</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_voucher" class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Voucher</span>
				</a>
			</li>
		</ul>
	</li>
		<!--
		<li class="nav-item  ">
			<a id="a_voucher" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Voucher</span>
			</a>
		</li>
		-->
		<br />
		<li class="nav-item  ">
			<a id="a_forum_internal" class="nav-link ">
				<i class="fa fa-wechat"></i>
				<span class="title">Forum Internal</span>
			</a>
		</li>
		<li class="nav-item  ">
			<a id="a_chat" class="nav-link ">
				<i class="fa fa-wechat"></i>
				<span class="title">Chat</span>
			</a>
		</li>
	<?php
	}
	?>
	
	
	<?php
	$menu_access = $_GET['menu_access'];
	if($menu_access == 'rekanan') {
	?>
		<li class="heading">
			<h3 class="uppercase">MENU REKANAN</h3>
		</li>
		<li class="nav-item  ">
			<a id="a_profile" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Profile</span>
			</a>
		</li>
		<li class="nav-item  ">
			<a href="javascript:;" class="nav-link nav-toggle">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Pra SPK</span>
				<span class="arrow"></span>
			</a>
			<ul class="sub-menu">
				<li class="nav-item  ">
					<a id="a_spk_type"  class="nav-link ">
						<i class="fa fa-sticky-note-o"></i>
						<span class="title">SPK Type</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="nav-item  ">
			<a id="a_spk" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Surat Perintah Kerja (SPK)</span>
			</a>
		</li>
		<li class="nav-item  ">
			<a href="javascript:;" class="nav-link nav-toggle">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Surat Instruksi</span>
				<span class="arrow"></span>
			</a>
			<ul class="sub-menu">
				<li class="nav-item  ">
					<a id="a_sik"  class="nav-link ">
						<i class="fa fa-sticky-note-o"></i>
						<span class="title">SIK</span>
					</a>
				</li>
				<li class="nav-item  ">
					<a id="a_sil"  class="nav-link ">
						<i class="fa fa-sticky-note-o"></i>
						<span class="title">SIL</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="nav-item  ">
			<a id="a_variation_order" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Variation Order (VO)</span>
			</a>
		</li>
		<li class="nav-item  ">
			<a id="a_progress_lapangan" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Progres Lapangan</span>
			</a>
		</li>
		<li class="nav-item  ">
			<a id="a_berita_acara_prestasi" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Berita Acara Prestasi (BAP)</span>
			</a>
		</li>
		<li class="nav-item  ">
			<a id="a_voucher" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Voucher</span>
			</a>
		</li>
		<li class="nav-item  ">
			<a id="a_purchase_order" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Purchase Order (PO)</span>
			</a>
		</li>
		<li class="nav-item  ">
			<a id="a_penerimaan_barang" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Penerimaan Barang</span>
			</a>
		</li>
		<li class="nav-item  ">
			<a id="a_good_receive" class="nav-link ">
				<i class="fa fa-sticky-note-o"></i>
				<span class="title">Good Receive (GR)</span>
			</a>
		</li>
	<?php
	}
	?>
	
	
	
	<!--
	<li class="heading">
		<h3 class="uppercase">INVENTORY</h3>
	</li>
	<li class="nav-item  ">
		<a id="a_inventarisasi" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Inventarisasi</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_pemakaian_barang" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Pemakaian Barang</span>
		</a>
	</li>
	
	<li class="heading">
		<h3 class="uppercase">PEKERJAAN</h3>
	</li>
	<li class="nav-item  ">
		<a id="a_work_order" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Work Order</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_rencana_anggaran_biaya" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Rencana Anggaran Biaya</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_rencana_anggaran_biaya" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Pra SPK(Tender/ Non Tender)</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_rencana_anggaran_biaya" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Surat Perintah Kerja (SPK)</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_rencana_anggaran_biaya" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Surah Instruksi Langsung (SIL)</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_rencana_anggaran_biaya" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Surat Instruksi Kontrak (SIK)</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_rencana_anggaran_biaya" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Variation Order (VO)</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_rencana_anggaran_biaya" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Progres Lapangan</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_rencana_anggaran_biaya" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Berita Acara Prestasi (BAP)</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_rencana_anggaran_biaya" class="nav-link ">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Voucher</span>
		</a>
	</li>
	
	
	<li class="heading">
		<h3 class="uppercase">MASTER PRIVILEDGE</h3>
	</li>
	<li class="nav-item  ">
		<a href="javascript:;" class="nav-link nav-toggle">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">User</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="nav-item  ">
				<a id="a_project_pt"  class="nav-link ">
					<i class="fa fa-sticky-note-o"></i>
					<span class="title">Project PT</span>
				</a>
			</li>
		</ul>
	</li>
	<li class="nav-item  ">
		<a id="a_jabatan"  class="nav-link ">
			<i class="icon-puzzle"></i>
			<span class="title">Jabatan</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_coa" href="javascript:;" class="nav-link nav-toggle">
			<i class="icon-puzzle"></i>
			<span class="title">COA</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_group_details"  class="nav-link ">
			<i class="icon-settings"></i>
			<span class="title">Group Details</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_group_privilidgebrowse"  class="nav-link ">
			<i class="icon-settings"></i>
			<span class="title">Group Priviledge Browse</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a id="a_menu_browse" class="nav-link ">
			<i class="icon-folder"></i>
			<span class="title">Menu Browse</span>
		</a>
	</li>
	<li class="nav-item  ">
		<a href="javascript:;" class="nav-link nav-toggle">
			<i class="icon-puzzle"></i>
			<span class="title">Wilayah</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub-menu">
			<li class="nav-item  ">
				<a id="a_countries" class="nav-link ">
					<i class="icon-puzzle"></i>
					<span class="title">Countries</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_province"  class="nav-link ">
					<i class="icon-puzzle"></i>
					<span class="title">Province</span>
				</a>
			</li>
			<li class="nav-item  ">
				<a id="a_cities"  class="nav-link ">
					<i class="icon-puzzle"></i>
					<span class="title">Cities</span>
				</a>
			</li>
		</ul>
	</li>
	<li class="nav-item  ">
		<a id="a_bank" href="javascript:;" class="nav-link nav-toggle">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Banks</span>
		</a>
	</li>
	
	<li class="heading">
		<h3 class="uppercase">PAGES</h3>
	</li>
	<li class="nav-item  ">
		<a id="a_login" href="javascript:;" class="nav-link nav-toggle">
			<i class="fa fa-sticky-note-o"></i>
			<span class="title">Login</span>
		</a>
	</li>
	-->
</ul>
<!-- END SIDEBAR MENU -->

<script type="text/javascript">
	$(document).ready(function(){
		// MASTER
		$("#a_item_pekerjaan").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('item_pekerjaan/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_barang").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('404.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_profile_rekanan").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('rekanans/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_supp").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('rekanans_supp/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_pt").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('pt/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_department").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('department/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		$("#a_project").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('project/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		$("#a_kawasan").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('project_kawasan/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		$("#a_project_blocks").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('project_blocks/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_unit").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('project_units/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_sub_unit").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('project_units_sub/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		
		
		$("#a_budged_total").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('budged_total/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_purchase_request").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('purchase_request/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_purchase_order").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('purchase_order/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_budged_tahunan").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('budged_tahunan/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		
		/*
		$("#a_item_pekerjaan_details").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('item_pekerjaan_details/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		*/
		
		// MASTER MANDATORY ACCESS
		$("#a_users").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('users/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_jabatan").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('jabatan/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_project_pt").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('project_pt/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_user_groups").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('user_groups/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_group_details").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('group_details/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_group_privilidgebrowse").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('group_priviledge_browse/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_menu_browse").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('menu_browse/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		
		$("#a_voucher").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('voucher/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_good_receive").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('good_receive/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		$("#a_spk_type").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('spk_type/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_rencana_anggaran_biaya").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('rencana_anggaran_biaya/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_work_order").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('work_order/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		$("#a_spk").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('spk/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		$("#a_retur").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('returs/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_berita_acara_prestasi").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('berita_acara_prestasi/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_variation_order").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('variation_order/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_sik").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('sik/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_sil").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('sil/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		/*
		$("#a_progress_lapangan").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('berita_acara_prestasi/index.php');
				$("#div_content").fadeIn('slow');
			});
		}); */
		$("#a_berita_acara_prestasi").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('berita_acara_prestasi/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		/*
		$("#a_group_privilidge").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('group_priviledge/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		*/
		$("#a_coa").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('coa/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_bank").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('bank/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_countries").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('countries/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_province").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('province/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		$("#a_cities").click(function(){
			$("#div_content").fadeOut('slow', function()
			{
				$("#div_content").load('cities/index.php');
				$("#div_content").fadeIn('slow');
			});
		});
		
		$('#a_login').click(function() {
		   window.open('user_login.html');   
		});
	});
</script>