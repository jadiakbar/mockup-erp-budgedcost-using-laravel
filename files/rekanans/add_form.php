<table class='table'>
	<tr>
		<td>Code</td>
		<td>
			<input type='text' id='code' name='code' class='form-control' placeholder='ID/ Code' required />
		</td>
	</tr>

	<tr>
		<td>Nama Rekanan</td>
		<td>
			<input type='text' id='nama_rekanan' name='nama_rekanan' class='form-control' placeholder='Nama Rekanan' required />
		</td>
	</tr>
	
	<tr>
		<td>Kelas</td>
		<td>
			<select class="form-control" name="kelas">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Pph (%)</td>
		<td>
			<input type='text' id='pph' name='pph' class='form-control' placeholder='PPH' required style='20%' />
		</td>
	</tr>
	
	<tr>
		<td>No NPWP</td>
		<td>
			<input type='text' id='kelas' name='kelas' class='form-control' placeholder='No NPWP' required style='20%' />
		</td>
	</tr>
	
	<tr>
		<td>Alamat</td>
		<td>
			<input type='text' id='alamat' name='alamat' class='form-control' placeholder='Alamat NPWP' required />
		</td>
	</tr>
	
	<tr>
		<td>Kota NPWP</td>
		<td>
			<select class='form-control' name='kota_npwp'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Alamat Surat</td>
		<td>
			<select class='form-control' name='alamat_surat'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Kota Surat</td>
		<td>
			<select class='form-control' name='kota_surat'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Email</td>
		<td>
			<input type='text' id='email' name='email' class='form-control' placeholder='email@email.com' required />
		</td>
	</tr>
	
	<tr>
		<td>Telephone</td>
		<td>
			<input type='text' id='telephone' name='telephone' class='form-control' placeholder='Telephone' required />
		</td>
	</tr>
	
	<tr>
		<td>Whatsapp</td>
		<td>
			<input type='text' id='whatsapp' name='whatsapp' class='form-control' placeholder='Whatsapp' required />
		</td>
	</tr>
	
	<tr>
		<td>Line</td>
		<td>
			<input type='text' id='line' name='line' class='form-control' placeholder='Line' required />
		</td>
	</tr>
	
	<tr>
		<td>Fax</td>
		<td>
			<input type='text' id='fax' name='fax' class='form-control' placeholder='Fax' required />
		</td>
	</tr>
	
	<tr>
		<td>Contact Person (CP)</td>
		<td>
			<input type='text' id='contact_person' name='contact_person' class='form-control' placeholder='Contact Person' required />
		</td>
	</tr>
	
	<tr>
		<td>No KTP CP</td>
		<td>
			<input type='text' id='no_ktp_cp' name='no_ktp_cp' class='form-control' placeholder='No KTP CP' required />
		</td>
	</tr>
	
	<tr>
		<td>Jabatan CP</td>
		<td>
			<input type='text' id='jabatan_cp' name='jabatan_cp' class='form-control' placeholder='Jabatan CP' required />
		</td>
	</tr>
	
	<tr>
		<td>No Line CP</td>
		<td>
			<input type='text' id='no_line_cp' name='no_line_cp' class='form-control' placeholder='Line CP' required />
		</td>
	</tr>
	
	<tr>
		<td>Status Survey</td>
		<td>
			<select class='form-control' name='status_survey' style="width:30%">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Tanggal Survey</td>
		<td>
			<input type='text' id='tgl_survey' name='tgl_survey' class='form-control' placeholder='Tanggal Survey' required />
		</td>
	</tr>
	
	<tr>
		<td>Tanggal PKP</td>
		<td>
			<input type='text' id='tgl_pkp' name='tgl_pkp' class='form-control' placeholder='Tanggal PKP' required style='width:30%' />
		</td>
	</tr>
	
	<tr>
		<td>Status PKP</td>
		<td>
			<select class='form-control' name='status_pkp' style="width:30%">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Status Aktif</td>
		<td>
			<select class='form-control' name='status_aktif' style="width:30%">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>STUJK</td>
		<td>
			<select class='form-control' name='stujk' style="width:30%">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Tanggal Gabung</td>
		<td>
			<select class='form-control' name='tgl_gabung' style="width:40%">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>User Group</td>
		<td>
			<select class='form-control' name='user_group' style="width:40%">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descritions" id="Descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>