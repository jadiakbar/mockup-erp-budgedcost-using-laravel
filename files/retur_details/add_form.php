
<table class='table'>
	<tr>
		<td>Barang Retur ID</td>
		<td>
			<label>(barang_retur.id)</label>
		</td>
	</tr>

	<tr>
		<td>Good Receive Detail ID</td>
		<td>
			<input type='text' id='grd_id' name='grd_id' class='form-control' placeholder='Good Receive Detail ID' required />
		</td>
	</tr>
	
	<tr>
		<td>Quantity</td>
		<td>
			<input type='text' id='qty' name='qty' class='form-control' placeholder='Quantity' required style='width:40%' />
		</td>
	</tr>
	
	<tr>
		<td>Price</td>
		<td>
			<input type='text' id='price' name='price' class='form-control' placeholder='Price' required style='width:40%' />
		</td>
	</tr>
	
	<tr>
		<td>PPN (%)</td>
		<td>
			<input type='text' id='ppn' name='pppn' class='form-control' placeholder='PPN' required style='width:30%' />
		</td>
	</tr>
	
	<tr>
		<td>PPH (%)</td>
		<td>
			<input type='text' id='pph' name='pph' class='form-control' placeholder='PPH' required style='width:30%' />
		</td>
	</tr>
</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>