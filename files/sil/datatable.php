
<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal</th>
			<th>No</th>
			<th>SPK</th>
			<th>Biaya</th>
			<th>Detail</th>
			<th>VO</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$no = 1;
		for($no = 1; $no <= 25; $no++)
		{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $no.'-'.date('m-Y'); ?></td>
			<td>00<?php echo $no; ?>/SIK/EST/VI/2017/CN/PRJ</td>
			<td>00<?php echo $no + 7; ?>/SPK/EST/VI/2017/CN/PRJ</td>
			<td>
				<?php if(($no % 2) == 0) {
						echo $bertambah;
					}
					else if(in_array($no, array(2,5,9), TRUE)) {
						echo '';
					}
					else {
						echo $berkurang;
					}
				?>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple detail-link"> 
					<i class="fa fa-newspaper-o">&nbsp;Detail</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple vo-link"> 
					<i class="fa fa-newspaper-o">&nbsp;VO</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

