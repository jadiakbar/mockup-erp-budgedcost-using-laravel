<script type="application/javascript">
$(document).ready(function(){
	$(".delete-link").click(function() {
		var id = $(this).attr("id");
		var del_id = id;
		var parent = $(this).parent("td").parent("tr");
		
		$.confirm({
			title: 'Confirm Delete ?',
			icon: 'fa fa-warning',
			content: 'Are you sure delete Key ID ' +del_id+ ' !',
			autoClose: 'cancelAction|8000',
			buttons: {
				deleteUser: {
					text: 'Delete',
					btnClass: 'btn-red any-other-class',
					action: function () {
						$.post('sil/delete.php', {'del_id':del_id}, function(data) {
							parent.fadeOut('slow');
						});	
						
						$("#div_message").html('<div class="custom-alerts alert alert-success fade in">Sucessfully delete on </div>');
					}
				},
				cancelAction: function () {
					
				}
			}
		});
		return false;
	});
	
	$(".edit-link").click(function() {
		var id = $(this).attr("id");
		var edit_id = id;
		
		$.confirm({
			title: 'Confirm Edit ?',
			icon: 'fa fa-edit',
			content: 'Are you sure edit Key ID '+edit_id+ ' !',
			autoClose: 'cancelAction|8000',
			buttons: {
				deleteUser: {
					text: 'Edit',
					btnClass: 'btn btn-info',
					action: function () {
						$(".content-loader").fadeOut('slow', function() {
							$(".content-loader").fadeIn('slow');
							$(".content-loader").load('sil/edit_form.php?edit_id='+edit_id);
							$("#btn-add").hide();
							$("#btn-view").show();
						});
					}
				},
				cancelAction: function () {
					
				}
			}
		});
		return false;
	});
	
	$(".detail-link").click(function() {
		var id = $(this).attr("id");
		//var view_id = id;
		var view_id = 2;
		
		$.confirm({
			title: 'Confirm Detail View ?',
			icon: 'fa fa-newspaper-o',
			content: 'Are you sure want to view detail Key ID '+view_id+ ' !',
			autoClose: 'cancelAction|8000',
			buttons: {
				deleteUser: {
					text: 'View',
					btnClass: 'btn btn-info',
					action: function () {
						$(".content-loader").fadeOut('slow', function() {
							$(".content-loader").fadeIn('slow');
							$(".content-loader").load('sil/detail_form.php?view_id='+view_id);
							$("#btn-add").hide();
							$("#btn-view").show();
						});
					}
				},
				cancelAction: function () {
					
				}
			}
		});
		return false;
	});
	
	$(".vo-link").click(function() {
		var id = $(this).attr("id");
		//var view_id = id;
		var view_id = 2;
		
		$.confirm({
			title: 'Confirm Detail View ?',
			icon: 'fa fa-newspaper-o',
			content: 'Are you sure want to view detail Key ID '+view_id+ ' !',
			autoClose: 'cancelAction|8000',
			buttons: {
				deleteUser: {
					text: 'View',
					btnClass: 'btn btn-info',
					action: function () {
						$(".content-loader").fadeOut('slow', function() {
							$(".content-loader").fadeIn('slow');
							$(".content-loader").load('variation_order/index.php?view_id='+view_id);
							$("#btn-add").hide();
							$("#btn-view").show();
						});
					}
				},
				cancelAction: function () {
					
				}
			}
		});
		return false;
	});
});
</script>