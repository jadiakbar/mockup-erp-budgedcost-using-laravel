<h3 class="page-title"> Edit Surat Instruksi Lapangan
</h3>

<table class='table'>
	<tr>
		<td>No. Surat Perintah Kerja</td>
		<td>
			<input type='text' id='no_sik' name='no_sik' class='form-control' placeholder='No. SIK' required value='001/SPK/EST/VI/2017/CN/PRJ' disabled />
		</td>
	</tr>

	<tr>
		<td>No. Surat Instruksi Lapangan</td>
		<td>
			<input type='text' id='no_sik' name='no_sik' class='form-control' placeholder='No. SIK' required value='111/SIL/EST/VI/2017/CN/PRJ' disabled />
		</td>
	</tr>

	<tr>
		<td>Perihal</td>
		<td>
			<input type='text' id='perihal' name='perihal' class='form-control' placeholder='Perihal' required value='Penambahan Kanopi' />
		</td>
	</tr>
	
	<tr>
		<td>Isi</td>
		<td>
			<textarea class='form-control' name="isi" id="isi" cols="45" rows="5" placeholder="Isi">Ini adalah contoh surat instruksi lapangan kepada ...</textarea>
		</td>
	</tr>
	
</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

