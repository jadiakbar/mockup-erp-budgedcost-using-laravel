<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>No. Budged Global</th>
			<th>No. Budged Tahunan</th>
			<th>Revision Of</th>
			<th>Project</th>
			<th>Tahun Anggaran</th>
			<th>Status</th>
			<th>Is Active</th>
			<th>Descriptions</th>
			<th>Status Approval</th>
			<th>View</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>(budged.no<?php echo $no; ?>)</td>
			<td>(budgedtahunans.no<?php echo $no; ?>)</td>
			<td>(budgedtahunans.parent_id)</td>
			<td>...</td>
			<td>...</td>
			<td>(budgedtahunans.status)</td>
			<td>...</td>
			<td>...</td>
			<td>
				<?php
				/*
				$approved = '<div class="progress progress-striped active">';
				$approved .= '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">';
				$approved .= '<span class="sr-only"> Approved </span>';
				$approved .= '</div></div>';
				
				$in_progress = '<div class="progress progress-striped active">';
				$in_progress .= '<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 50%">';
				$in_progress .= '<span class="sr-only"> In Progress </span>';
				$in_progress .= '</div></div>';
				
				$rejected = '<div class="progress progress-striped">';
				$rejected .= '<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">';
				$rejected .= '<span class="sr-only"> Rejected </span>';
				$rejected .= '</div></div>';
				
				$approved = '<div class="alert-success fade in">Approved</div>';
				$in_progress = '<div class="alert-danger fade in">In Progress</div>';
				$rejected = '<div class="alert-warning fade in">In Rejected</div>';
				*/
				?>
			
				<?php if(($no % 2) == 0) {
					echo $approved;
				}
				else if(in_array($no, array(2,5,9), TRUE)) {
					echo $in_progress;
				}
				else {
					echo $rejected;
				}
				?>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>