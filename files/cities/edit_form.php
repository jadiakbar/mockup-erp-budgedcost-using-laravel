<table class='table'>
	<tr>
		<td>Province ID</td>
		<td>
			<select class='form-control' name="province_id">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>

	<tr>
		<td>Name</td>
		<td>
			<input type='text' id='name' name='name' class='form-control' placeholder='Cities Name' required />
		</td>
	</tr>
	
	<tr>
		<td>Zip Code</td>
		<td>
			<input type='text' id='zip_code' name='zip_code' class='form-control' placeholder='Zip Code' required />
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>