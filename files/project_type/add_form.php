<table class='table'>
	<tr>
		<td>Group Type</td>
		<td>
			<input type='text' id='group_type' name='group_type' class='form-control' placeholder='Group Type' value="EDUCATION" required />
		</td>
	</tr>

	<tr>
		<td>Code</td>
		<td>
			<input type='text' id='code' name='code' class='form-control' placeholder='ID/ Code' required />
		</td>
	</tr>

	<tr>
		<td>Name</td>
		<td>
			<input type='text' id='name' name='name' class='form-control' placeholder='Name' required />
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
	<tr>
		<td>Is Unit</td>
	  <td>
			<input type='checkbox' name='is_unit' checked />
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>