

<table class='table'>
	<tr>
		<td>Item</td>
		<td>
			<select class='form-control' name='item_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Satuan</td>
		<td>
			<select class='form-control' name='satuan_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Quantity</td>
		<td>
			<input type='text' id='quantity' name='quantity' class='form-control' placeholder='Quantity' required style='width:40%' />
		</td>
	</tr>
	
	<tr>
		<td>Price</td>
		<td>
			<input type='text' id='price' name='price' class='form-control' placeholder='Price' required style='width:40%' />
		</td>
	</tr>
	
	<tr>
		<td>PPN (%)</td>
		<td>
			<input type='text' id='ppn' name='ppn' class='form-control' placeholder='PPN (%)' required style='width:25%' />
		</td>
	</tr>
	
	<tr>
		<td>PPH (%)</td>
		<td>
			<input type='text' id='pph' name='pph' class='form-control' placeholder='PPH (%)' required style='width:25%' />
		</td>
	</tr>
	
</table>



<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php // include('multi_select_pluggin.php'); ?>