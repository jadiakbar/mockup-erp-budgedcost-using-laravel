<table class="table table-striped table-bordered table-hover table-responsive table-checkable" id="table_data_units">
	<thead>
		<tr style='text-align:center'>
			<th>No</th>
			<th>Unit</th>
			<th>COA</th>
			<th>Nilai</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>AA001</td>
			<td>11.42.311</td>
			<td style='text-align:right'>500,000,000.00</td>
			<td align="center">
				
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs default unit-link"> 
					<i class="fa fa-newspaper-o">&nbsp;Unit</i>
				</a>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>PPN 10%</td>
			<td>11.40.311</td>
			<td style='text-align:right'>50,000,000.00</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>PPh 2%</td>
			<td>11.40.312</td>
			<td style='text-align:right'>10,000,000.00</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>PPh 1%</td>
			<td>11.40.312</td>
			<td style='text-align:right'>5,000,000.00</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		
		<tr>
			<td></td>
			<td><strong></strong></td>
			<td><strong></strong></td>
			<td style='text-align:right'><strong></strong></td>
			<td align="center">
				
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs green add-unit-link"> 
					<i class="fa fa-edit">Add Rows</i>
				</a>
			</td>
		</tr>
		
		<tr>
			<td></td>
			<td><strong>Total</strong></td>
			<td><strong></strong></td>
			<td style='text-align:right'><strong>535,000,000.00</strong></td>
			<td align="center">
				
			</td>
			<td align="center">
			
			</td>
		</tr>
	</tbody>
</table>

