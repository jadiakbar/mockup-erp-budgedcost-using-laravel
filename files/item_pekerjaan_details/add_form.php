<table class='table'>
	<tr>
		<td>Item Pekerjaan </td>
		<td>
			<input type='text' id='item_pekerjaan' name='item_pekerjaan' class='form-control' placeholder='Item Pekerjaan Name' required />
		</td>
	</tr>
	
	<tr>
		<td>Nilai Nominal </td>
		<td>
			<input type='text' id='nilai_nominal' name='nilai_nominal' class='form-control' placeholder='Nilai Nominal' required style="width:50%" />
		</td>
	</tr>
	
	<tr>
		<td>Satuan </td>
		<td>
			<select class='form-control' name="satuan">
				<option>-- CHOOSE --</option>
				<option>Meter</option>
				<option>Kilogram</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Volume </td>
		<td>
			<input type='text' id='volume' name='volume' class='form-control' placeholder='Volume' required style="width:30%" />
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>