

<table class='table'>
	<tr>
		<td>Purchase Order Cancellation No.</td>
		<td>
			<select class='form-control' name='purchase_order_cancellation_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Purchase Order Details</td>
		<td>
			<select class='form-control' name='purchase_order_detail_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Quantity</td>
		<td>
			<input type='text' id='quantity' name='quantity' class='form-control' placeholder='Quantity' required style='width:20%' />
		</td>
	</tr>
	
	<tr>
		<td>Description</td>
		<td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
</table>



<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php // include('datetimepicker_pluggin.php'); ?>