<?php include('datatable_pluggin.php'); ?>

<div id="div_content_page">
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btn-view").hide();
			
			$("#btn-add").click(function(){
				$(".content-loader").fadeOut('slow', function()
				{
					$(".content-loader").fadeIn('slow');
					$(".content-loader").load('returs/add_form.php');
					$("#btn-add").hide();
					$("#btn-view").show();
				});
			});
			
			$("#btn-view").click(function(){
				
				$("#div_content_page").fadeOut('slow', function()
				{
					$("#div_content_page").load('returs/index.php');
					$("#div_content_page").fadeIn('slow');
				});
			});
		});
	</script>




	<div class="portlet-body">
		<h3 class="page-title">Returs
			<small>returs information</small>
		</h3>
		<hr />
			<button class="btn sbold green" type="button" id="btn-add"> <span class="glyphicon glyphicon-pencil"></span> &nbsp; Add Data</button>
			<button class="btn btn-info" type="button" id="btn-view" style="display:none"> <span class="glyphicon glyphicon-eye-open"></span> &nbsp; View Data</button>
		<hr />
		
		<div id="div_message">
			<!-- here message will be displayed -->
		</div>
		
		<div class="content-loader">
			<?php include('datatable.php'); ?>
		</div>
    </div>
    <br />

	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#table_data').DataTable();
			$('#table_data')
			.removeClass( 'display' )
			.addClass('table table-bordered');
		});
	</script>
</div>


<div id="div_ajax_func"> 
	<?php include('ajax_load.php'); ?> 
</div>


