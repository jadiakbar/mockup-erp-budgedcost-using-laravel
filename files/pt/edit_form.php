<table class='table'>
	<tr>
		<td>Code</td>
		<td>
			<input type='text' id='code' name='code' class='form-control' placeholder='ID/ Code' required />
		</td>
	</tr>
	
	<tr>
		<td>PT. Name</td>
		<td>
			<input type='text' id='pt_name' name='pt_name' class='form-control' placeholder='PT. Name' required />
		</td>
	</tr>
	
	<tr>
		<td>NPWP No.</td>
		<td>
			<input type='text' id='npwp_no' name='npwp_no' class='form-control' placeholder='No. NPWP' required />
		</td>
	</tr>
	
	<tr>
		<td>Phone</td>
		<td>
			<input type='text' id='phone' name='phone' class='form-control' placeholder='Phone' required style="width:30%" />
		</td>
	</tr>
	
	<tr>
		<td>Rekening No.</td>
		<td>
			<input type='text' id='rek_no' name='rek_no' class='form-control' placeholder='No. Rekening' required />
		</td>
	</tr>
	
	<tr>
		<td>City</td>
		<td>
			<input type='text' id='city' name='city' class='form-control' placeholder='City' required />
		</td>
	</tr>
	
	<tr>
		<td>Address</td>
	  <td>
			<textarea class='form-control' name="address" id="address" cols="45" rows="5" placeholder="Address"></textarea>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>