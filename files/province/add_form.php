<table class='table'>
	<tr>
		<td>Country ID</td>
		<td>
			<select class='form-control' name="country_id">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>

	<tr>
		<td>Name</td>
		<td>
			<input type='text' id='name' name='name' class='form-control' placeholder='Province Name' required />
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="Descritions" id="descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>