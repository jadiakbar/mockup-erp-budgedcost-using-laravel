<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Menu ID</th>
			<th>Menu Name</th>
			<th>Add</th>
			<th>Edit</th>
			<th>Delete</th>
			<th>View</th>
			<th>Visible</th>
			<th>Descriptions</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>MN00<?php echo $no; ?></td>
			<td>Menu Name <?php echo $no; ?></td>
			<td>
				<?php if(($no % 2) == 0) {
					echo '<span class="font-blue-steel">TRUE</span>';
				}
				else {
					echo '<span class="font-red">FALSE</span>';
				}
				?>
			</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo '<span class="font-blue-steel">TRUE</span>';
				}
				else {
					echo '<span class="font-red">FALSE</span>';
				}
				?>
			</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo '<span class="font-blue-steel">TRUE</span>';
				}
				else {
					echo '<span class="font-red">FALSE</span>';
				}
				?>
			</td>
			<td><span class="font-blue-steel">TRUE</span></td>
			<td><span class="font-blue-steel">TRUE</span></td>
			<td>-</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

