<table class='table'>
	<tr>
		<td>Group Name</td>
		<td colspan="5">
			<select class='form-control' name="group_name">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Menus</td>
		<td colspan="5">
			<select class='form-control' name="menus">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Actions</td>
		<td>Add
			<input type="checkbox" name="add" checked />
		</td>
		<td>Edit
			<input type="checkbox" name="edit" checked /></td>
		<td>Delete
			<input type="checkbox" name="delete" checked /></td>
		<td>View
			<input type="checkbox" name="view" checked /></td>
		<td>Visible
			<input type="checkbox" name="visible" checked /></td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td colspan="5">
			<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>