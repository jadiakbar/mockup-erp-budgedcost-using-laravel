
<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal</th>
			<th>No</th>
			<th>Nilai</th>
			<th>Detail</th>
			<th>SPK</th>
			<th>Voucher</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td><?php echo $no; ?>/10/2017</td>
			<td>00<?php echo $no; ?>/BAP/EST/VI/2017/CN/PRJ</td>
			<td align='right'><?php echo number_format($no * 500000,2); ?></td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple detail-link"> 
					<i class="fa fa-newspaper-o">&nbsp;Detail</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue spk-link"> 
					<i class="fa fa-newspaper-o">&nbsp;Spk</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue voucher-link"> 
					<i class="fa fa-edit">Voucher</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

