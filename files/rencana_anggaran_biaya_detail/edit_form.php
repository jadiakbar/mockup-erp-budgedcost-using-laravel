

<table class='table'>
	<tr>
		<td>RAB No.</td>
		<td>
			<input type='text' id='work_order_no' name='work_order_no' class='form-control' placeholder='(rabs.no)' required />
		</td>
	</tr>
	
	<tr>
		<td>Search Unit</td>
		<td>
			<div class="input-group">
				<input class="form-control" placeholder="Search" type="text">
					<span class="input-group-addon">
					<i class="fa fa-search"></i>
					</span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Units</td>
		<td>
			<div class="form-group">
				<div class="col-md-9">
					<select multiple="multiple" class="multi-select" id="my_multi_select1" name="no_spk[]" onchange='setTermin()'>
					<?php
					$no = 1;
					for($no = 1; $no <= 10; $no++)
					{
						?>
						<option value='#<?php echo $no; ?>'>
						(units.code<?php echo $no; ?>)
						</option>
						<?php
					}
					?>
					</select>
				</div>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Description</td>
		<td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
</table>



<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php include('multi_select_pluggin.php'); ?>