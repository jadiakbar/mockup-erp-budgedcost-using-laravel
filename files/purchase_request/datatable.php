<!--<h3 class="page-title"> Detail Nama Rekanan 1
</h3>
-->

<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>PT. Name</th>
			<th>Department</th>
			<th>Location</th>
			<th>No.</th>
			<th>Tanggal</th>
			<th>Tgl Kebutuhan</th>
			<th>Status</th>
			<th>Descriptions</th>
			<th>Detail</th>
			<th>Cancellation</th>
			<th>Detail Penawaran</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>PT. Name <?php echo $no; ?></td>
			<td>Department Name <?php echo $no; ?></td>
			<td>Location Name <?php echo $no; ?></td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo $open;
				}
				else if(in_array($no, array(2,5,9), TRUE)) {
					echo $approved;
				}
				else if(in_array($no, array(3,7), TRUE)) {
					echo $partial;
				}
				else {
					echo $closed;
				}
				?>
			</td>
			<td>...</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple detail-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple cancellation-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple detail_penawaran-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

