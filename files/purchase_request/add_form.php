<table class='table'>
	<tr>
		<td>PT Name</td>
		<td>
			<select class='form-control' name='pt_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>

	<tr>
		<td>Department</td>
		<td>
			<select class='form-control' name='department_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Location</td>
		<td>
			<select class="form-control" name="location_id">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>No</td>
		<td>
			<input type='text' id='no' name='no' class='form-control' placeholder='No.' required />
		</td>
	</tr>
	
	<tr>
		<td>Tanggal</td>
		<td>
			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control" name='date' readonly required value="<?php echo date('d-m-Y'); ?>">
				<span class="input-group-btn">
				<button class="btn default" type="button">
				<i class="fa fa-calendar"></i>
				</button>
				</span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Tanggal Butuh</td>
		<td>
			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control" name='butuh_date' readonly required value="<?php echo date('d-m-Y'); ?>">
				<span class="input-group-btn">
				<button class="btn default" type="button">
				<i class="fa fa-calendar"></i>
				</button>
				</span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Status</td>
		<td>
			<select class="form-control" name="location_id">
				<option>-- CHOOSE --</option>
				<option>Open</option>
				<option>Approved</option>
				<option>Partial</option>
				<option>Closed</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
		<td>
			<input type='text' id='alamat' name='alamat' class='form-control' placeholder='Alamat NPWP' required />
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php include('datetimepicker_pluggin.php'); ?>