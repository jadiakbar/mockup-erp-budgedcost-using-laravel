

<table class='table'>
	<tr>
		<td>Purchase Request Detail ID.</td>
		<td>
			<input type='text' id='work_order_no' name='work_order_no' class='form-control' placeholder='(rabs.no)' required />
		</td>
	</tr>
	
	<tr>
		<td>Rekanan ID</td>
		<td>
			<select class='form-control' name='rekanan_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Price</td>
		<td>
			<input type='text' id='price' name='price' class='form-control' placeholder='Price' style='width:20%' required />
		</td>
	</tr>
	
	<tr>
		<td>Description</td>
		<td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
</table>



<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>
