<h3 class="page-title"> Create Variation Order
</h3>

<table class='table'>
	<tr>
		<td>No. SPK</td>
		<td>
			<div class="form-group">
				<div class="col-md-9">
					<select multiple="multiple" class="multi-select" id="my_multi_select1" name="no_spk[]" onchange='setTermin()'>
					<?php
					$no = 1;
					for($no = 1; $no <= 10; $no++)
					{
						?>
						<option value='#<?php echo $no; ?>'>
						00<?php echo $no; ?>/SPK/EST/VI/2017/CN/PRJ</option>
						<?php
					}
					?>
					</select>
				</div>
			</div>
		</td>
	</tr>

	<tr>
		<td>No. SIK</td>
		<td>
			<div class="form-group">
				<div class="col-md-9">
					<select multiple="multiple" class="multi-select" id="my_multi_select3" name="no_spk[]">
					<?php
					$no = 1;
					for($no = 1; $no <= 10; $no++)
					{
						?>
						<option value='#<?php echo $no; ?>'>
						00<?php echo $no; ?>/SIK/EST/VI/2017/CN/PRJ</option>
						<?php
					}
					?>
					</select>
				</div>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Urutan</td>
		<td>
			<input type='text' id='urutan' name='urutan' class='form-control' placeholder='Urutan' required value='Ke - 1' disabled />
		</td>
	</tr>
	
	<tr>
		<td>Tanggal</td>
		<td>
			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control" name='tanggal' readonly required value="<?php echo date('d-m-Y'); ?>" >
					<span class="input-group-btn">
				<button class="btn default" type="button">
					<i class="fa fa-calendar"></i>
				</button>
				</span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Keterangan</td>
		<td>
			<textarea class='form-control' name="keterangan" id="keterangan" cols="45" rows="5" placeholder="Keterangan"></textarea>
		</td>
	</tr>
	
	<tr>
		<td><strong><h3>Unit</h3></strong></td>
		<td>
			
		</td>
	</tr>
	
	<tr>
		<td colspan='2'>
		<?php
			for($i = 1; $i <= 3; $i++) {
			?>
				<table>
					<tr>
						<td>
							<a onclick='hideUnit<?php echo $i; ?>()'>AA00<?php echo $i; ?>[show/hide]</a>
						</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
				
				
				<table class="table table-striped table-bordered table-hover table-responsive table-checkable" id="table_data_units<?php echo $i; ?>">
				  <tr>
					<td style='width:20%'><input type="checkbox" name="item_pekerjaan" id="item_pekerjaan" />
						<label for="item_pekerjaan">&nbsp; Item Pekerjaan Satu<?php echo $i; ?></label></td>
					<td><input type="text" class='form-control' name="keterangan" id="keterangan" placeholder="Keterangan" /></td>
					<td>
						<input type="text" class='form-control' name="nilai_satuan" id="nilai_satuan" placeholder="Nilai Satuan" />
					</td>
					<td><input type="text" class='form-control' name="volume" id="volume" placeholder='Volume' /></td>
					<td>
						<select class='form-control' name='satuan'>
							<option>-- CHOOSE --</option>
							<option>Meter</option>
							<option>Kilogram</option>
						</select>
					</td>
					<td><input type="text" class='form-control' name="nilai_real" id="nilai_real" placeholder='Nilai Real' /></td>
				  </tr>
				  <tr>
					<td><input type="checkbox" name="item_pekerjaan" id="item_pekerjaan" />
						<label for="item_pekerjaan">&nbsp; Item Pekerjaan Dua<?php echo $i; ?></label></td>
					<td><input type="text" class='form-control' name="keterangan" id="keterangan" placeholder="Keterangan" /></td>
					<td><input type="text" class='form-control' name="nilai_satuan" id="nilai_satuan" placeholder="Nilai Satuan" /></td>
					<td><input type="text" class='form-control' name="volume" id="volume" placeholder='Volume' /></td>
					<td><input type="text" class='form-control' name="satuan" id="satuan" placeholder='Satuan' /></td>
					<td><input type="text" class='form-control' name="nilai_real" id="nilai_real" placeholder='Nilai Real' /></td>
				  </tr>
				  <tr>
					<td><input type="checkbox" name="item_pekerjaan" id="item_pekerjaan" />
						<label for="item_pekerjaan">&nbsp; Item Pekerjaan Tiga<?php echo $i; ?></label></td>
					<td><input type="text" class='form-control' name="keterangan" id="keterangan" placeholder="Keterangan" /></td>
					<td><input type="text" class='form-control' name="nilai_satuan" id="nilai_satuan" placeholder="Nilai Satuan" /></td>
					<td><input type="text" class='form-control' name="volume" id="volume" placeholder='Volume' /></td>
					<td><input type="text" class='form-control' name="satuan" id="satuan" placeholder='Satuan' /></td>
					<td><input type="text" class='form-control' name="nilai_real" id="nilai_real" placeholder='Nilai Real' /></td>
				  </tr>
				</table>
				
				<script>
				function hideUnit<?php echo $i; ?>() {
					var table_data_units = document.getElementById('table_data_units<?php echo $i; ?>');
					
					var hide = table_data_units.style.display =="none";
					if (hide) {
						table_data_units.style.display="table";
					} 
					else {
						table_data_units.style.display="none";
					}
				}
				</script>
			<?php
			}
			?>
		</td>
	</tr>
</table>

<div align='right'>
	<a id="" href="javascript:;" class="btn btn-xs green add-unit-link"> 
		<i class="fa fa-plus">Add Rows</i>
	</a>
</div>

<?php include('multi_select_pluggin.php'); ?>
<?php include('datetimepicker_pluggin.php'); ?>
<script type="text/javascript" charset="utf-8">
	/*
	$(document).ready(function() {
		$('#table_data_units').DataTable();
		$('#table_data_units')
		.removeClass( 'display' )
		.addClass('table table-bordered');
	});
	*/
</script>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

