
<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>No. WO</th>
			<th>Name</th>
			<th>Tanggal</th>
			<th>Dept. Asal</th>
			<th>Dept. Tujuan</th>
			<th>Budged Tahunan</th>
			<th>Project</th>
			<th>Durasi</th>
			<th>St. Waktu</th>
			<th>Est. Nilai WO</th>
			<th>Description</th>
			<th>Status</th>
			<th>RAB</th>
			<th>Detail</th> 
			<th>Edit</th>
			<th>Delete</th>
			<th>Sirkulasi</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$no = 1;
		for($no = 1; $no <= 25; $no++)
		{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>WO00<?php echo $no; ?></td>
			<td>(workorder.name)</td>
			<td><?php echo $no.'-'.date('m-Y') ?></td>
			<td>(departmentasal.name<?php echo $no; ?>)</td>
			<td>(departmenttujuan.name<?php echo $no; ?>)</td>
			<td>...</td>
			<td>Project <?php echo $no; ?></td>
			<td>
				<?php if(($no % 2) == 0) {
						echo '24';
					}
					else {
						echo '30';
					}
				?>
			</td>
			<td>...</td>
			<td>...</td>
			<td>(description_workorder<?php echo $no; ?></td>
			<td align="center">
				<?php
				/*
				$approved = '<div class="alert-success fade in">Approved</div>';
				$in_progress = '<div class="alert-danger fade in">In Progress</div>';
				$rejected = '<div class="alert-warning fade in">In Rejected</div>';
				*/
				?>
			
				<?php if(($no % 2) == 0) {
					echo $approved;
				}
				else if(in_array($no, array(2,5,9), TRUE)) {
					echo $in_progress;
				}
				else {
					echo $rejected;
				}
				?>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue rab-link"> 
					<i class="fa fa-edit">&nbsp;RAB</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue detail-link"> 
					<i class="fa fa-edit">&nbsp;Detail</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">&nbsp;Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">&nbsp;Delete</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs yellow sirkulasi-link">
					<i class="fa-sticky-note">&nbsp;Sirkulasi</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

