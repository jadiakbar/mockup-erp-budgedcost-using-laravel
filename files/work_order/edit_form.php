<h3 class="page-title"> Create Variation Order
</h3>

<table class='table'>
	<tr>
		<td>Project</td>
		<td>
			<input type='text' id='project' name='project' class='form-control' placeholder='Project' required value='(projetname from session login)' disabled />
		</td>
	</tr>
	
	<tr>
		<td>Budged Tahunan</td>
		<td>
			<select class='form-control' name='budged_tahunan'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>WO Num</td>
		<td>
			<input type='text' id='wo_num' name='wo_num' class='form-control' placeholder='WO Num' required style='width:70%' />
		</td>
	</tr>
	
	<tr>
		<td>WO Name</td>
		<td>
			<input type='text' id='wo_name' name='wo_name' class='form-control' placeholder='WO Name' required />
		</td>
	</tr>
	
	<tr>
		<td>Tanggal</td>
		<td>
			<select class='form-control' name='tanggal'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>

	<tr>
		<td>Department Asal</td>
		<td>
			<select class='form-control' name='dept_asal'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Department Tujuan</td>
		<td>
			<select class='form-control' name='dept_tujuan'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Est. Nilai WO</td>
		<td>
			<select class='form-control' name='est_nilai_wo'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Durasi</td>
		<td>
			<input type='text' id='wo_name' name='durasi' class='form-control' placeholder='WO Name' required style='width:30%' />
		</td>
	</tr>
	
	<tr>
		<td>Satuan Waktu</td>
		<td>
			<select class='form-control' name='satuan_waktu' style='width:30'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Desciption</td>
		<td>
			<textarea class='form-control' name="description" id="description" cols="45" rows="5" placeholder="Description"></textarea>
		</td>
	</tr>
</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

