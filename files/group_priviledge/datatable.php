<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>User Group ID</th>
			<th>Menu ID</th>
			<th>Action</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>Administrator</td>
			<td>001</td>
			<td>
				Add<input class='checker' type="checkbox" name="add" id="add">
				Edit<input class='checker' type="checkbox" name="edit" id="add">
				Delete<input class='checker' type="checkbox" name="delete" id="add">
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<tr>
			<td>2</td>
			<td>User</td>
			<td>002</td>
			<td>
				Add<input class='checker' type="checkbox" name="add" id="add">
				Edit<input class='checker' type="checkbox" name="edit" id="add">
				Delete<input class='checker' type="checkbox" name="delete" id="add">
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
	</tbody>
</table>


<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>
