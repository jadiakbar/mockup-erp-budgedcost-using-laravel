

<table class='table'>
	<tr>
		<td>Purchase Request No.</td>
		<td>
			<select class='form-control' name='purchase_request_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Item Pekerjaan</td>
		<td>
			<select class='form-control' name='item_pekerjaan_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Item</td>
		<td>
			<select class='form-control' name='item_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Item Satuan</td>
		<td>
			<select class='form-control' name='item_satuan_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Brand</td>
		<td>
			<select class='form-control' name='brand_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Recomended Supplier</td>
		<td>
			<input type='text' id='recomended_supplier' name='recomended_supplier' class='form-control' placeholder='Recomended Supplier' required />
		</td>
	</tr>
	
	<tr>
		<td>Quantity</td>
		<td>
			<input type='text' id='quantity' name='quantity' class='form-control' placeholder='Quantity' required style='width:20%' />
		</td>
	</tr>
	
	<tr>
		<td>Description</td>
		<td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
</table>



<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php // include('multi_select_pluggin.php'); ?>