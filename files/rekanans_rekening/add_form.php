<table class='table'>
	<tr>
		<td>Kode Rekanan </td>
		<td>
			<input type='text' id='kode_rekanan' name='kode_rekanan' class='form-control' placeholder='Kode Rekanan' required />
		</td>
	</tr>
	
	<tr>
		<td>Nama Rekanan</td>
		<td>
			<select class='form-control' name="nama_rekanan">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Bank </td>
		<td>
			<select class='form-control' name="bank">
				<option>-- CHOOSE --</option>
				<option>BCA</option>
				<option>BRI</option>
				<option>BNI</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>No. Rekening </td>
		<td>
			<input type='text' id='no_rekening' name='no_rekening' class='form-control' placeholder='Nomor Rekening' required style="width:30%" />
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>