<table class='table'>
	<tr>
		<td>Project</td>
		<td>
			<label>Project Name</label>
		</td>
	</tr>

	<tr>
		<td>PT</td>
		<td>
			<select class='form-control' name='pt'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Department</td>
		<td>
			<select class='form-control' name='department'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Kawasan</td>
		<td>
			<select class='form-control' name='kawasan'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>No. Budged Global</td>
		<td>
			<input type='text' id='no_budged_global' name='no_budged_global' class='form-control' placeholder='No. Budged Global' required />
		</td>
	</tr>
	
	<tr>
		<td>Start Date</td>
		<td>
			<select class='form-control' name='start_date'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Finish Date</td>
		<td>
			<select class='form-control' name='finish_date'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Status</td>
		<td>
			<select class='form-control' name='status' style='widht:30%'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
	<tr>
		<td>Revision Of</td>
		<td>
			<select class='form-control' name='revision_of'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Is Active</td>
		<td>
			<input type='checkbox' name='is_active' checked />
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>