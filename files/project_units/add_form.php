<table class='table'>
	<tr>
		<td>Code</td>
		<td>
			<input type='text' id='code' name='code' class='form-control' placeholder='Code' required style="widht:30%" />
		</td>
	</tr>
	
	<tr>
		<td>Name</td>
		<td>
			<input type='text' id='name' name='name' class='form-control' placeholder='Name' required />
		</td>
	</tr>
	
	<tr>
		<td>Luas Tanah</td>
		<td>
			<input type='text' id='luas_tanah' name='luas_tanah' class='form-control' placeholder='Luas Tanah' required />
		</td>
	</tr>
	
	<tr>
		<td>HPP Tanah/meter</td>
		<td>
			<input type='text' id='hpp_tanah' name='hpp_tanah' class='form-control' placeholder='HPP Tanah' required style="widht:30%" />
		</td>
	</tr>
	
	<tr>
		<td>HPP Bangunan/meter</td>
		<td>
			<input type='text' id='hpp_bangunan' name='hpp_bangunan' class='form-control' placeholder='HPP Bangunan' required style="widht:30%" />
		</td>
	</tr>
	
	<tr>
		<td>Peruntukan</td>
		<td>
			<select class='form-control' name='peruntukan'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Tanggal ST1</td>
		<td>
			<select class='form-control' name='tgl_st1' style='width:50%'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Tanggal ST2</td>
		<td>
			<select class='form-control' name='tgl_st2' style='width:50%'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>PT</td>
		<td>
			<select class='form-control' name='pt'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Status</td>
		<td>
			<select class='form-control' name='status' style='width:50%'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Arah Hadap</td>
		<td>
			<select class='form-control' name='arah_hadap'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Temp. Pekerjaan</td>
		<td>
			<select class='form-control' name='temp_pekerjaan'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>
	
	<tr>
		<td>Sellable</td>
		<td>
			<input type='checkbox' name='sellable' checked />
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>