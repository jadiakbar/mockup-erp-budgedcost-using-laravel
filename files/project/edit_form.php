<table class='table'>
	<tr>
		<td>Add Unit</td>
		<td>
			<select class='form-control' name='add_unit'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>

	<tr>
		<td>Code</td>
		<td>
			<input type='text' id='code' name='code' class='form-control' placeholder='ID/ Code' required required style='width:25%'  />
		</td>
	</tr>

	<tr>
		<td>Name</td>
		<td>
			<input type='text' id='name' name='name' class='form-control' placeholder='Project Name' required />
		</td>
	</tr>
	
	<tr>
		<td>Luas</td>
		<td>
			<input type='text' id='luas' name='luas' class='form-control' placeholder='Luas' required style='width:30%' />
		</td>
	</tr>
	
	<tr>
		<td>NPWP</td>
		<td>
			<input type='text' id='npwp' name='npwp' class='form-control' placeholder='NPWP No.' required />
		</td>
	</tr>
	
	<tr>
		<td>Phone</td>
		<td>
			<input type='text' id='phone' name='phone' class='form-control' placeholder='Phone' required />
		</td>
	</tr>
	
	<tr>
		<td>Fax</td>
		<td>
			<input type='text' id='fax' name='fax' class='form-control' placeholder='Fax No.' required />
		</td>
	</tr>
	
	<tr>
		<td>Email</td>
		<td>
			<input type='text' id='email' name='email' class='form-control' placeholder='Email' required />
		</td>
	</tr>
	
	<tr>
		<td>Contact Person</td>
		<td>
			<select class='form-control' name='contact_person'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Address</td>
		<td>
			<textarea class='form-control' name="address" id="address" cols="45" rows="5" placeholder="Address"></textarea>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
	  <td>
			<textarea class='form-control' name="descriptions" id="descriptions" cols="45" rows="5" placeholder="Descriptions"></textarea>
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>