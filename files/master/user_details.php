<table class='table'>
		<tr>
            <td>Perusahaan Identity</td>
            <td>
            	<select class='form-control' name="mappingperusahaan_id">
            	  <option value="101">PT. ABC</option>
            	  <option value="102">PT. Kediri</option>
            	  <option value="103">PT. Mojokerto</option>
          	  	</select>
			</td>
        </tr>
	
        <tr>
            <td>Jabatan</td>
            <td>
            	<select class='form-control' name="user_level">
            	  <option value="1">Manager</option>
            	  <option value="2">Supervisor</option>
            	  <option value="3">Pegawai Lapangan</option>
          	  </select>
            </td>
        </tr>
 
        <tr>
            <td>Level</td>
            <td><select class='form-control' name="user_level">
              <option value="1">Super Administrator</option>
              <option value="2">Administrator</option>
              <option value="3">Employee</option>
            </select></td>
        </tr>
 
        <tr>
            <td>Approval Request</td>
            <td><input class='checker' type="checkbox" name="can_approve" id="can_approve">
            <label for="can_approve"></label></td>
        </tr>
        
        <tr>
            <td>Descriptions</td>
          <td><label for="descritions"></label>
				<textarea class='form-control' name="descritions" id="descritions" cols="45" rows="5"></textarea>
			</td>
        </tr>
 
</table>