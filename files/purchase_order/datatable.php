<!--<h3 class="page-title"> Detail Nama Rekanan 1
</h3>
-->

<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Purchase Request No</th>
			<th>Rekanan</th>
			<th>Location</th>
			<th>Tanggal</th>
			<th>Mata Uang</th>
			<th>Kurs</th>
			<th>Term Pengiriman</th>
			<th>Cara Pembayaran</th>
			<th>Status</th>
			<th>Description</th>
			<th>Detail</th>
			<th>Cancellation</th>
			<th>DPS</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$no = 1;
	for($no = 1; $no <= 25; $no++)
	{
		?>
		<tr>
			<td><?php echo $no; ?></td>
			<td>(00<?php echo $no; ?>/PO/EST/VI/2017/CN/PRJ)</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>...</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo $cash;
				}
				else if(in_array($no, array(2,9), TRUE)) {
					echo $credit;
				}
				else if(in_array($no, array(3,7), TRUE)) {
					echo $transfer;
				}
				else if(in_array($no, array(5,8), TRUE)) {
					echo $cek;
				}
				else {
					echo $giro;
				}
				?>
			</td>
			<td>
				<?php if(($no % 2) == 0) {
					echo $open;
				}
				else if(in_array($no, array(2,9), TRUE)) {
					echo $approved;
				}
				else if(in_array($no, array(3,7), TRUE)) {
					echo $partial;
				}
				else if(in_array($no, array(5,8), TRUE)) {
					echo $paid;
				}
				else {
					echo $closed;
				}
				?>
			</td>
			<td>...</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple detail-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple cancellation-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs purple dps-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

