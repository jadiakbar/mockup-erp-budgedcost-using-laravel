<table class='table'>
	<tr>
		<td>Purchase Order</td>
		<td>
			<select class='form-control' name='purchaseorder_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>

	<tr>
		<td>Rekanan</td>
		<td>
			<select class='form-control' name='rekanan_id'>
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Location</td>
		<td>
			<select class="form-control" name="location_id">
				<option>-- CHOOSE --</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>No</td>
		<td>
			<input type='text' id='no' name='no' class='form-control' placeholder='No.' required style='20%' />
		</td>
	</tr>
	
	<tr>
		<td>Tanggal</td>
		<td>
			<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				<input type="text" class="form-control" name='date' readonly required value="<?php echo date('d-m-Y'); ?>">
				<span class="input-group-btn">
				<button class="btn default" type="button">
				<i class="fa fa-calendar"></i>
				</button>
				</span>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>Mata Uang</td>
		<td>
			<select class="form-control" name="matauang">
				<option>-- CHOOSE --</option>
				<option>Rupiah</option>
				<option>Dolar</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Kurs</td>
		<td>
			<input type='text' id='kurs' name='kurs' class='form-control' placeholder='Kurs' required style='width:30%' />
		</td>
	</tr>
	
	<tr>
		<td>Term Pengiriman (Hari)</td>
		<td>
			<input type='text' id='term_pengiriman' name='term_pengiriman' class='form-control' placeholder='Term Pengiriman' required style='width:25%' /> 
		</td>
	</tr>
	
	<tr>
		<td>Cara Pembayaran</td>
		<td>
			<select class="form-control" name="carapembayaran">
				<option>-- CHOOSE --</option>
				<option>Cash</option>
				<option>Credit</option>
				<option>Transfer</option>
				<option>Cek</option>
				<option>Giro</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Status</td>
		<td>
			<select class="form-control" name="status">
				<option>-- CHOOSE --</option>
				<option>Open</option>
				<option>Approved</option>
				<option>Partial</option>
				<option>Paid</option>
				<option>Closed</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td>Descriptions</td>
		<td>
			<input type='text' id='description' name='description' class='form-control' placeholder='Descriptions' required />
		</td>
	</tr>

</table>

<div align="center">
	<button type="button" class="btn green showtoast" name="btn-save" id="showtoast">
		<span class="fa fa-save"></span> Save this Record
	</button>
</div>

<?php include('datetimepicker_pluggin.php'); ?>