<table class="table table-striped table-bordered table-hover table-responsive table-checkable order-column" id="table_data">
	<thead>
		<tr>
			<th>No</th>
			<th>Code</th>
			<th>Name</th>
			<th>Descriptions</th>
			<th>View</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>HS</td>
			<td>HOUSING</td>
			<td>...</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<tr>
			<td>2</td>
			<td>CM</td>
			<td>COMMERCIAL</td>
			<td>...</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<tr>
			<td>3</td>
			<td>ED</td>
			<td>EDUCATION</td>
			<td>...</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<tr>
			<td>3</td>
			<td>PU</td>
			<td>PUBLIC UTILITY</td>
			<td>...</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
		<tr>
			<td>3</td>
			<td>OT</td>
			<td>OTHERS</td>
			<td>...</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue view-link"> 
					<i class="fa fa-newspaper-o">&nbsp;View</i>
				</a>
			</td>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs blue edit-link"> 
					<i class="fa fa-edit">Edit</i>
				</a>
			</td>
			<td align="center">
				<a id="" href="javascript:;" class="btn btn-xs red delete-link"> 
					<i class="fa fa-times">Delete</i>
				</a>
			</td>
		</tr>
	</tbody>
</table>

